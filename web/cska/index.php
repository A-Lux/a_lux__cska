<?php require_once('header.php'); ?>

<div class="online-pay wow slideInRight">
	<a class="online-pay-btn" href="#">Онлайн покупка</a>

	<ul>
		<li><a href="#">Покупка книг</a></li>
		<li><a href="#" data-toggle="modal" data-target="#myModal">Бронирование экскурсий</a></li>
		<li><a href="#">Приобретение билетов</a></li>
		<li><a href="#">Покупка сувенирной продукции</a></li>
	</ul>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="feedback-form modal-content">
			<form action="">
				<a class="feedback-close" data-dismiss="modal">
					<img src="images/close-icon.png" alt="">
				</a>
				<div class="title">Оставить заявку</div>
				<input type="text" placeholder="Ваше имя*">
				<input class="phone_us" type="text" placeholder="Номер телефона*">
				<input type="text" placeholder="Почта*">
				<textarea type="text" placeholder="Ваше сообщение*"></textarea>
				<div class="contacts-btn no-top mid">
					<a href="#">Обратная связь</a>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Модальное окно -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="main">

	<div class="main-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5 col-md-5 no-padding responsive-bg">
					<div class="main-left wow slideInLeft">
						<h1>Нет Востока, <br> И Запада нет. <br> Есть восход и закат, <br> Есть большое слово — ЗЕМЛЯ!</h1>

						<ul>
							<li><a href="#">Купить билет</a></li>
							<li><a href="#">Узнать больше о мероприятии</a></li>
							<li><a href="#">Отзывы о мероприятии</a></li>
							<li><a href="#">Еще какая-то ссылка</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-7 col-md-7 main-right wow slideInRight"></div>
			</div>
		</div>

	</div>

	<div class="main-about-center wow slideInUp">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5 no-padding main-about-center-left-bg">
					<div class="main-about-center-left">
						<h2>О центре</h2>
						<a href="#">Узнать больше</a>
					</div>
				</div>

				<div class="col-lg-7 no-padding main-about-center-right-wrap">
					<div class="main-about-center-right">
						<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития на диалоговой площадке Центра.
							Центр путем установления и развития профессиональных контактов с партнерскими учреждениями как внутри центрально-азиатского региона, так и за его пределами, в том числе с религиозными объединениями, научно-исследовательскими и аналитическими институтами, ведущими высшими учебными заведениями, этнокультурными центрами и НПО проводит работу по
							изучение и сохранение многовекового опыта мирного сосуществования и взаимообогащения культур народов, проживающих на территории Центральной Азии</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="main-activity">
			<div class="row">
				<div class="col-lg-5">
					<div class="main-activity-title wow bounceInLeft">
						<h2>Ближайшие мероприятия</h2>
					</div>
				</div>
			</div>

			<div class="offset-lg-7"></div>
			<div class="row">
				<div class="col-lg-5">
					<div class="main-activity-tabs">
						<ul>
							<li><a href="#tab1">Все</a></li>
							<li><a href="#tab2">Конференции</a></li>
							<li><a href="#tab3">Выставки</a></li>
							<li><a href="#tab4">Экскурсии</a></li>
						</ul>
					</div>
				</div>

				<div class="offset-lg-8"></div>
			</div>

			<div class="main-activity-items">
				<div class="row">

					<div class="col-lg-9 no-padding">
						<div class="main-activity-tabs-wrap wow bounceInUp">
							<div class="main-activity-tabs-items" id="tab1">
								<!-- TAB 1 -->
								<div class="main-activity-item">
									<img src="images/name-conf.jpg" alt="conf">
									<h4>Название конференции</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Конференция</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition.jpg" alt="exhibition">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Экскурсия</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition-2.jpg" alt="exhibition-2">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Выставка</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>
							</div>
							<!-- END TAB 1 -->

							<!-- TAB 2 -->
							<div class="main-activity-tabs-items" id="tab2">
								<div class="main-activity-item">
									<img src="images/name-conf.jpg" alt="conf">
									<h4>Tab 2</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Конференция</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition.jpg" alt="exhibition">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Экскурсия</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition-2.jpg" alt="exhibition-2">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Выставка</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>
							</div>
							<!-- END TAB 2 -->

							<!-- TAB 3 -->
							<div class="main-activity-tabs-items" id="tab3">
								<div class="main-activity-item">
									<img src="images/name-conf.jpg" alt="conf">
									<h4>Tab 3</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Конференция</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition.jpg" alt="exhibition">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Экскурсия</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition-2.jpg" alt="exhibition-2">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Выставка</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>
							</div>
							<!-- END TAB 3 -->

							<!-- TAB 4 -->
							<div class="main-activity-tabs-items" id="tab4">
								<div class="main-activity-item">
									<img src="images/name-conf.jpg" alt="conf">
									<h4>Tab 4</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Конференция</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition.jpg" alt="exhibition">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Экскурсия</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>

								<div class="main-activity-item">
									<img src="images/name-exhibition-2.jpg" alt="exhibition-2">
									<h4>Название выставки</h4>
									<a href="#">24 февраля 2019</a>
									<h3>Выставка</h3>
									<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития </p>
								</div>
							</div>
							<!-- END TAB 4 -->
						</div>
					</div>



					<!-- 4 -->
					<div class="col-lg-3">
						<div class="main-recommended wow slideInRight">
							<h3>Рекомендуемые</h3>
							<div class="main-recommended-1">
								<div class="main-recommended-item">
									<span>24 февраля 2019</span>
									<h5>Выставка</h5>
									<h4>Название конференции</h4>
								</div>
							</div>

							<div class="main-recommended-2">
								<div class="main-recommended-item">
									<span>24 февраля 2019</span>
									<h5>Выставка</h5>
									<h4>Название конференции</h4>
								</div>
							</div>

							<div class="main-recommended-3">
								<div class="main-recommended-item">
									<span>24 февраля 2019</span>
									<h5>Выставка</h5>
									<h4>Название конференции</h4>
								</div>
							</div>
						</div>
					</div>
					<!-- END 4 -->

				</div>
			</div>
		</div>
	</div>

	<!-- БИЛЕТЫ НА ВЫСТАВКИ -->
	<div class="tickets-wrap wow bounceInUp">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="tickets-left">
						<h2>Билеты на выставки, конференции и экскурсии. А также покупка книг.</h2>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="tickets-right">
						<p>Главной функцией Центра стало осуществление координации активного межкультурного диалога ученых и экспертов по широкому кругу вопросов в области устойчивого культурного развития на диалоговой площадке Центра.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END БИЛЕТЫ НА ВЫСТАВКИ -->


	<!-- НОВОСТИ -->
	<div class="container">

		<div class="main-news wow slideInUp">

			<div class="row">
				<div class="col-lg-3">
					<div class="news-title">
						<h2>Новости</h2>
					</div>
				</div>

				<div class="offset-lg-9"></div>
			</div>

			<div class="row">
				<div class="col-lg-3 col-md-3">
					<div class="news-items-small">
						<img src="images/news-1.jpg" alt="Новости">
						<div class="fixed-news-text">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>

					</div>
				</div>

				<div class="col-lg-6 col-md-6">
					<div class="news-items-big">
						<div class="news-items-big-content">
							<img src="images/news-2.jpg" alt="Новости">
							<div class="news-items-big-content-span">
								<h5>Развитие человеческих ресурсов путем подготовки</h5>
								<span>24 февраля 2019</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3">
					<div class="news-items-small">
						<img src="images/news-3.jpg" alt="Новости">
						<div class="fixed-news-text">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>

					</div>
				</div>
			</div>

			<div class="news-text">
				<div class="row">

					<div class="col-lg-3 news-text-items-wrap">
						<div class="news-text-items news-text-items-1">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="news-text-items">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="news-text-items">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="news-text-items">
							<h5>Развитие человеческих ресурсов путем подготовки</h5>
							<span>24 февраля 2019</span>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	<!-- END НОВОСТИ -->

	<!-- КОНТАКТЫ -->
	<div class="container-fluid">

		<div class="main-contacts">
			<div class="row">
				<div class="col-lg-7 no-padding">
					<div class="contacts-left wow slideInLeft">
						<iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.1499163638327!2d76.92339789079226!3d43.248279756730646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836eca1e6f2ed1%3A0xb8591feee8117fb9!2z0YPQuy4g0JrQsNCx0LDQvdCx0LDQuSDQsdCw0YLRi9GA0LAgOTQsINCQ0LvQvNCw0YLRiyAwNTAwMDA!5e0!3m2!1sru!2skz!4v1548305115059" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-lg-5 no-padding">
					<div class="contacts-right wow slideInRight">
						<div class="contacts-right-title">
							<h2>Контакты</h2>
						</div>

						<div class="contacts-location contacts-inner-items">
							<img src="images/main-location.png" alt="Локация">
							<span>Алматы л.Кабанбай батыра, 94</span>
						</div>

						<div class="contacts-phone contacts-inner-items">
							<img src="images/main-phone.png" alt="Телефон">
							<span>(727) 261-20-09, 261-16-92</span>
						</div>

						<div class="contacts-message contacts-inner-items">
							<img src="images/main-message.png" alt="Сообщение">
							<span>muzei@mail.ru, crc-kz@mail.ru</span>
						</div>

						<div class="contacts-btn">
							<a href="#">Обратная связь</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END КОНТАКТЫ -->
</div>

<?php require_once('footer.php'); ?>