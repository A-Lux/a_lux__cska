<?php require_once('header.php');?>


    <!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <span>/</span>
                    <li><a href="#">Экспозиция </a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="#">Интерактивная карта</a></li>
                </ul>
            </div>

        </div>
        <div class="gallery-title">
            <h2>Интерактивная  карта <br>«Великий Шелковый путь»</h2>
        </div>

        <div class="container">
            <div class="mandat-content text-center">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="map-img">
                <a href="">
                    <img src="images/map.jpg" alt="">
                    <div class="img-caption">
                        <p>Виртуальный тур по нашему музею</p>
                    </div>
                </a>
            </div>
            <div class="back-main space-bot">
                <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
            </div>
        </div>

    </div>
    <!-- END MAIN -->

<?php require_once('footer.php');?>