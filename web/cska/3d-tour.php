<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="#">Главная</a></li>
				<span>/</span>
				<li><a href="#">Экспозиция</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">3D Тур</a></li>
			</ul>
		</div>

	</div>

	<div class="container">
		
		<div class="tur-wrap">
			
			<div class="tur-title">
				<h2>3D Тур</h2>
			</div>

			<div class="tur-text">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est </p>
			</div>

			<div class="tur-item">
				<iframe src="https://www.google.com/maps/embed?pb=!4v1548823415439!6m8!1m7!1sCAoSLEFGMVFpcE9CaU1XengzRVpkTjFZcXpPMVh6X0Jjb0ZCbER3UWZFcW9SMUtv!2m2!1d48.69623890409189!2d44.50748107203397!3f275.49!4f-26.25!5f0.48818408613213227" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>

		</div>

		<div class="cska-pagination">
				<ul>
					<li><a href="#">В начало</a></li>
					<li><a href="#">Назад</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">Вперёд</a></li>
					<li><a href="#">В конец</a></li>
				</ul>
			</div>

		<div class="back-main">
			<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
		</div>

	</div>

	<!-- END MAIN -->



<?php require_once('footer.php');?>