<?php require_once('header.php');?>

  <main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
        <ul>
          <li><a href="index.php">Главная</a></li>
          <span>/</span>
          <li><a href="#">Мероприятия/Проекты</a></li>
          <span>/</span>
          <li><a class="bread-crumbs-active" href="articles.php">Статьи/Интервью</a></li>
        </ul>
      </div>
      <div class="middle-title">
        <h3>Cтатьи, интервью</h3>
      </div>

      <div class="books-main">
        <ul class="tabs">
          <li class="tab-link current" data-tab="tab-1"> 2018</li>
          <li class="tab-link" data-tab="tab-2">2017</li>
          <li class="tab-link" data-tab="tab-3">2016</li>
        </ul>

        <div id="tab-1" class="tab-content current">
			
          <div class="article-item">
            <p>Олжас Сулейменов размышляет о предстоящем в Астане Втором мировом конгрессе тюркологов <a target="_blank" href="https://express-k.kz/news/politika_expert/olzhas_suleymenov_razmyshlyaet_o_predstoyashchem_v_astane_vtorom_mirovom_kongresse_tyurkologov-136224">https://express-k.kz/news/politika_expert/olzhas_suleymenov_razmyshlyaet_o_predstoyashchem_v_astane_vtorom_mirovom_kongresse_tyurkologov-136224</a> трансляция состоялась 25 июля 2017 г.;</p>
          </div>
			
		  <div class="article-item">
            <p>Олжас Сулейменов: если государства и общества объединят усилия, человечество достигнет своей цели – спасти мир от войны <a target="_blank" href="https://express-k.kz/news/politika_expert/olzhas_suleymenov_esli_gosudarstva_i_obshchestva_obedinyat_usiliya_chelovechestvo_dostignet_svoey_ts-132802">https://express-k.kz/news/politika_expert/olzhas_suleymenov_esli_gosudarstva_i_obshchestva_obedinyat_usiliya_chelovechestvo_dostignet_svoey_ts-132802</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>
        </div>

        <div id="tab-2" class="tab-content">
          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda laudantium, error reiciendis eum aspernatur rem odit eligendi. Excepturi, dolor, maxime. на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a target="_blank" href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>
        </div>

        <div id="tab-3" class="tab-content">
          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>
        </div>
      </div>
      <div class="cska-pagination">
        <ul>
          <li><a href="#">В начало</a></li>
          <li><a href="#">Назад</a></li>
          <li><a href="#">1</a></li>
          <li class="active"><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Вперёд</a></li>
          <li><a href="#">В конец</a></li>
        </ul>
      </div>

      <a href="#" class="return-main-page">
        Вернуться на главную
      </a>
    </div>
  </main>


<?php require_once('footer.php');?>
