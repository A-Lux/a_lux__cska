<?php require_once('header.php');?>


<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">О центре</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Организационная структура</a></li>
			</ul>
		</div>

	</div>

	<div class="reports-wrap">
		<div class="container h-100">
			
			<div class="reports-title">
				<h2>Отчеты</h2>
			</div>

		</div>
	</div>

	<div class="container">
		<div class="reports-content">

			<div class="report-document-wrap">
				<div class="report-document-item">
					<img src="images/document.png" alt="document">
					<a href="#">Годовой отчет за 2018 год</a>
				</div>

				<div class="report-document-item">
					<img src="images/document.png" alt="document">
					<a href="#">Годовой отчет за 2018 год</a>
				</div>

				<div class="report-document-item">
					<img src="images/document.png" alt="document">
					<a href="#">Годовой отчет за 2018 год</a>
				</div>

				<div class="back-main">
					<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
				</div>
			</div>

		</div>
	</div>

</div>
<!-- END MAIN -->

<?php require_once('footer.php');?>