<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задача, цели и мандат</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
	<link rel="stylesheet" href="css/superfish.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/jquery.bxslider.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<script src="js/jquery-3.2.1.min.js"></script>
</head>

<body>
	
	<!-- preloader -->
	<script type="text/javascript">
		$(function(){
			$("#cube-loader").fadeOut("slow");
		});
	</script>


	<div id="cube-loader">
		<div class="caption">
			<div class="cube-loader">
				<div class="cube loader-1"></div>
				<div class="cube loader-2"></div>
				<div class="cube loader-4"></div>
				<div class="cube loader-3"></div>
			</div>
		</div>
	</div>
	<!-- end preloader -->
	
	<!-- HEADER -->
	<header class="header" id="header">

		<div class="container no-padding">
			<!-- TOP -->
			<div class="header-top wow fadeInLeft">
				<div class="header-logo">
					<a href="index.php"><img src="images/logo.jpg" alt="Логотип"></a>
				</div>

				<div class="header-items-wrap">
					<div class="header-phone header-items">
						<h6>(727) 261-20-09</h6>
						<h6>(727) 261-16-92</h6>
					</div>

					<div class="header-items-mes-loc">
						<div class="header-message header-items">
							<h6>muzei@mail.ru</h6>
						</div>

						<div class="header-location header-items">
							<h6>г. Алматы. ул. Кабанбай батыра, 94</h6>
						</div>
					</div>
					
					<div class="header-lang" id="header-lang">
							<a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="images/main-lang.png" alt="RU">
							</a>
						
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">
								<img src="images/main-lang-en.png" alt="RU">
								<span>English</span>
							</a>
							<a class="dropdown-item" href="#">
								<img src="images/main-lang-kz.png" alt="RU">
								<span>Қазақ</span>
							</a>
						</div>

					</div>
				</div>
			</div>
			<!-- END TOP -->	
		</div>

		<!-- MENU -->
		<div class="header-menu wow slideInRight">
			
			<div class="container no-padding">
				<div class="hamburger-menu-btn">
					<a href="#">
						<img src="images/menu-button.svg" alt="menu button">
					</a>
				</div>

				
				<!-- MOBILE MENU -->
				<nav class="mobile-menu">
					<ul>
						<li><a href="index.php">Главная</a></li>
						<li>
							<a class="submenu-arrow submenu-head" href="#">О Центре</a>
							<ul class="mobile-submenu">
								<li><a href="mandat.php">Задачи, цели, мандат</a></li>
								<li><a href="org-structure.php">Организационная структура</a></li>
								<li><a href="reports.php">Отчеты</a></li>
							</ul>
						</li>

						<li>
							<a class="submenu-arrow submenu-head" href="#">Мероприятия/Проекты</a>
							<ul class="mobile-submenu">
								<li><a href="grant-projects.php">Грантовые проекты</a></li>
								<li><a href="conference.php">Конференции/Симпозиумы</a></li>
								<li><a href="forums.php">Форумы</a></li>
								<li><a href="exhibitions.php">Выставки</a></li>
								<li>
									<a class="submenu-arrow mobile-submenu-link" href="#">Публикации</a>
									<ul class="mobile-submenu-sub">
										<li><a href="articles.php">Статьи, интервью</a></li>
										<li><a href="books.php">Книги</a></li>
									</ul>
								</li>
							</ul>
						</li>

						<li><a href="partners.php">Партнеры</a></li>

						<li>
							<a class="submenu-arrow submenu-head" href="#">Новости</a>
							<ul class="mobile-submenu">
								<li><a href="events.php">События</a></li>
								<li><a href="news.php">Новости Министерства культуры и спорта РК</a></li>
							</ul>
						</li>

						<li>
							<a class="submenu-arrow submenu-head" href="#">Медиа</a>
							<ul class="mobile-submenu">
								<li><a href="gallery.php">Галерея</a></li>
								<li><a href="mass-media.php">СМИ о нас</a></li>
								<li><a href="press-releases.php">Пресс релизы</a></li>
							</ul>
						</li>

						<li>
							<a class="submenu-arrow submenu-head" href="#">Экспозиция</a>
							<ul class="mobile-submenu">
								<li><a href="ex-structure.php">Структура</a></li>
								<li><a href="collections.php">Коллекции</a></li>
								<li><a href="3d-tour.php">3D тур</a></li>
								<li>
									<a class="submenu-arrow mobile-submenu-link" href="#">Интерактивная карта «Великий Шелковый путь»</a>
									<ul class="mobile-submenu-sub" style="left: -100%;">
										<li><a href="map.php">"Шелковый путь в Казахстане"</a></li>
										<li><a href="books.php">"Шелковый путь во всем мире"</a></li>
									</ul>
								</li>
								<li><a href="souvenir-main.php">Cувенирная продукция</a></li>
								<li><a href="price-list.php">Прейскурант</a></li>
							</ul>
						</li>

						<li>
							<a class="submenu-arrow submenu-head" href="#">Контакты</a>
							<ul class="mobile-submenu">
								<li><a href="contacts.php">Обратная связь</a></li>
								<li><a href="vacancy.php">Вакансии</a></li>
								<li><a href="blog.php">Блог директора</a></li>
							</ul>
						</li>

					</ul>
				</nav>
				<!-- END MOBILE MENU -->

				<!-- DESKTOP MENU -->
				<nav class="menu">
					<ul class="sf-menu">
						<li><a href="index.php">Главная</a></li>
						<li>
							<a href="#">О Центре</a>
							<ul class="submenu">
								<li><a href="mandat.php">Задачи, цели, мандат</a></li>
								<li><a href="org-structure.php">Организационная структура</a></li>
								<li><a href="reports.php">Отчеты</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Мероприятия/Проекты</a>
							<ul class="submenu">
								<li><a href="grant-projects.php">Грантовые проекты</a></li>
								<li><a href="conference.php">Конференции/Симпозиумы</a></li>
								<li><a href="forums.php">Форумы</a></li>
								<li><a href="exhibitions.php">Выставки</a></li>
								<li>
									<a href="#">Публикации</a>
									<ul class="submenu-sub">
										<li><a href="articles.php">Статьи, интервью</a></li>
										<li><a href="books.php">Книги</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li><a href="partners.php">Партнеры</a></li>
						<li>
							<a href="#">Новости</a>
							<ul class="submenu">
								<li><a href="events.php">События</a></li>
								<li><a href="news.php">Новости Министерства культуры и спорта РК</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Медиа</a>
							<ul class="submenu">
								<li><a href="gallery.php">Галерея</a></li>
								<li><a href="mass-media.php">СМИ о нас</a></li>
								<li><a href="press-releases.php">Пресс релизы</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Экспозиция</a>
							<ul class="submenu">
								<li><a href="ex-structure.php">Структура</a></li>
								<li><a href="collections.php">Коллекции</a></li>
								<li><a href="3d-tour.php">3D тур</a></li>
								<li>
									<a href="">Интерактивная карта «Великий Шелковый путь»</a>
									<ul class="submenu-sub" style="left: -100%;">
										<li><a href="map.php">"Шелковый путь в Казахстане"</a></li>
										<li><a href="books.php">"Шелковый путь во всем мире"</a></li>
									</ul>
								</li>
								<li><a href="souvenir-main.php">Cувенирная продукция</a></li>
								<li><a href="price-list.php">Прейскурант</a></li>
							</ul>
					</li>
						<li>
							<a href="">Контакты</a>
							<ul class="submenu">
								<li><a href="contacts.php">Обратная связь</a></li>
								<li><a href="vacancy.php">Вакансии</a></li>
								<li><a href="blog.php">Блог директора</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- END DESKTOP MENU -->
			</div>

		</div>
		<!-- END MENU -->
	</header>
	<!-- END HEADER -->