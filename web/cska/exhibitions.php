<?php require_once('header.php');?>

<main class="main">
  <div class="container">
    <div class="bread-crumbs wow fadeInLeft">
      <ul>
        <li><a href="index.php">Главная</a></li>
        <span>/</span>
        <li><a href="#">Мероприятия/Проекты</a></li>
        <span>/</span>
        <li><a class="bread-crumbs-active" href="exhibitions.php">Выставки</a></li>
      </ul>
    </div>
    <div class="middle-title">
      <h3>Выставки</h3>
    </div>

    <div class="collection-wrapper">
      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>

      <a href="exhibitions-inner.php" class="collection-item">
        <div class="collection-img">
          <img src="images/conferences.png">
        </div>
        <p>Развитие человеческих
ресурсов путем подготовки</p>
        <span>24 февраля 2019</span>
      </a>
    </div>

    <div class="cska-pagination">
      <ul>
        <li><a href="#">В начало</a></li>
        <li><a href="#">Назад</a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">Вперёд</a></li>
        <li><a href="#">В конец</a></li>
      </ul>
    </div>

    <a href="#" class="return-main-page">
      Вернуться на главную
    </a>
  </div>
</main>


<?php require_once('footer.php');?>
