<?php require_once('header.php');?>

  <main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
        <ul>
          <li><a href="index.php">Главная</a></li>
          <span>/</span>
          <li><a href="#">Контакты</a></li>
          <span>/</span>
          <li><a class="bread-crumbs-active" href="#">Вакансии</a></li>
        </ul>
      </div>
      <div class="middle-title">
        <h3>Вакансии</h3>
      </div>

      <div class="vacancy-wrapper">
        <div class="vacancy-item">
          <h3>Название вакансии</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <div class="price">
            <p><span>000 000 тг</span> / За месяц</p>
          </div>
          <ul>
            <li><a href="#">Алматы, Бостандыкский район</a></li>
            <li><a>9 дек.	Полный день</a></li>
            <li><a>Полная занятость</a></li>
          </ul>
        </div>

        <div class="vacancy-item">
          <h3>Название вакансии</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <div class="price">
            <p><span>000 000 тг</span> / За месяц</p>
          </div>
          <ul>
            <li><a href="#">Алматы, Бостандыкский район</a></li>
            <li><a>9 дек.	Полный день</a></li>
            <li><a>Полная занятость</a></li>
          </ul>
        </div>

        <div class="vacancy-item">
          <h3>Название вакансии</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <div class="price">
            <p><span>000 000 тг</span> / За месяц</p>
          </div>
          <ul>
            <li><a href="#">Алматы, Бостандыкский район</a></li>
            <li><a>9 дек.	Полный день</a></li>
            <li><a>Полная занятость</a></li>
          </ul>
        </div>

        <div class="vacancy-item">
          <h3>Название вакансии</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <div class="price">
            <p><span>000 000 тг</span> / За месяц</p>
          </div>
          <ul>
            <li><a href="#">Алматы, Бостандыкский район</a></li>
            <li><a>9 дек.	Полный день</a></li>
            <li><a>Полная занятость</a></li>
          </ul>
        </div>
      </div>

      <div class="cska-pagination">
        <ul>
          <li><a href="#">В начало</a></li>
          <li><a href="#">Назад</a></li>
          <li><a href="#">1</a></li>
          <li class="active"><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Вперёд</a></li>
          <li><a href="#">В конец</a></li>
        </ul>
      </div>

      <a href="#" class="return-main-page">
        Вернуться на главную
      </a>
    </div>
  </main>

  <?php require_once('footer.php');?>
