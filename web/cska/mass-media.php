<?php require_once('header.php');?>



  <main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
        <ul>
          <li><a href="#">Главная</a></li>
          <span>/</span>
          <li><a href="#">Медиа</a></li>
          <span>/</span>
          <li><a class="bread-crumbs-active" href="#">СМИ о нас</a></li>
        </ul>
      </div>
      <div class="middle-title">
        <h3>СМИ о нас</h3>
      </div>

      <div class="books-main">
        <ul class="tabs">
          <li class="tab-link current" data-tab="tab-1"> 2018</li>
          <li class="tab-link" data-tab="tab-2">2017</li>
          <li class="tab-link" data-tab="tab-3">2016</li>
        </ul>

        <div id="tab-1" class="tab-content current">
          <div class="article-item">
            <p>Өсімталдық мінез сыры. Калиева С.А. Газета «Қазақ әдебиеты». 16.06.2017 №25 (3555).- С.12</p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», программа «Новости», ссылка <a href="http://24.kz/ru/news/social/item/188554-v-almaty-zavershaetsya-rekonstruktsiya">http://24.kz/ru/news/social/item/188554-v-almaty-zavershaetsya-rekonstruktsiya</a> трансляция состоялась 25 июля 2017 г.;</p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>
        </div>

        <div id="tab-2" class="tab-content">
          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda laudantium, error reiciendis eum aspernatur rem odit eligendi. Excepturi, dolor, maxime. на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» на русском, казахском языке.Материалы Международная научно-профилактическая конференция «Религии Казахстана и Центральной Азии на Великом шелковом пути», выпуск на русском языке от 19 июня 2017 г: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n</a>, выпуск на казахском языке от 19 июня 2017г.: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>

          <div class="article-item">
            <p>Телеканал «Хабар», утренняя передача «Жана кун» материалы Международного симпозиума «Великий Шелковый путь: диалог и сближения культур средневековья (путешествие Гильома де Рубрука 1253 год)» г. Астана, 4 сентября ЭКСПО 2017 г., от 14 сентября 2017 г., на русском языке, ссылка: <a href="http://khabar.kz/kz/telezhobalar/zha-a-k-n">http://khabar.kz/kz/telezhobalar/zha-a-k-n;</a></p>
          </div>
        </div>

        <div id="tab-3" class="tab-content">
          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat voluptatem quasi ipsum ex libero culpa sapiente et adipisci, tenetur esse voluptate quam nisi ratione rerum modi!</p>
          </div>

          <div class="article-item">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nam, nulla tenetur hic quo ullam atque tempora facere! Ipsam deserunt dolore eum necessitatibus sit quibusdam nihil, sint nobis sed, maxime tenetur, velit non. Itaque repellat sed sapiente porro consequatur consequuntur, iste quisquam beatae aspernatur fuga!</p>
          </div>
        </div>
      </div>
      <div class="cska-pagination">
        <ul>
          <li><a href="#">В начало</a></li>
          <li><a href="#">Назад</a></li>
          <li><a href="#">1</a></li>
          <li class="active"><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Вперёд</a></li>
          <li><a href="#">В конец</a></li>
        </ul>
      </div>

      <a href="#" class="return-main-page">
        Вернуться на главную
      </a>
    </div>
  </main>


<?php require_once('footer.php');?>
