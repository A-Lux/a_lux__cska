<?php require_once('header.php');?>


    <!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="index.php">Главная</a></li>
                    <span>/</span>
                     <li><a href="#">Контакты</a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="#">Обратная связь</a></li>
                </ul>
            </div>

        </div>
        <div class="gallery-title">
            <h2>Контакты</h2>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="feedback">
                        <h3>Обратная связь</h3>
                    </div>

                    <div class="contacts-location contacts-inner-items">
                        <img src="images/main-location.png" alt="Локация">
                        <span>Алматы л.Кабанбай батыра, 94</span>
                    </div>

                    <div class="contacts-phone contacts-inner-items">
                        <img src="images/main-phone.png" alt="Телефон">
                        <span>(727) 261-20-09, 261-16-92</span>
                    </div>

                    <div class="contacts-message contacts-inner-items">
                        <img src="images/main-message.png" alt="Сообщение">
                        <span>muzei@mail.ru, crc-kz@mail.ru</span>
                    </div>
                    <div class="socials">
                        <a href=""><img src="images/soc1.png" alt=""></a>
                        <a href=""><img src="images/soc2.png" alt=""></a>
                        <a href=""><img src="images/soc3.png" alt=""></a>
                        <a href=""><img src="images/soc4.png" alt=""></a>
                        <a href=""><img src="images/soc5.png" alt=""></a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <form action="">
                        <div class="contact">
                            <textarea type="text" placeholder="Ваше сообщение*"></textarea> <br>
                            <input type="text" placeholder="Ваше имя*"> <br>
                            <input type="text" placeholder="Ваше e-mail*">
                        </div>
                        <div class="contacts-btn no-top">
                            <a href="#">Отправить</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!-- END MAIN -->
    <div class="map">
        <iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.1499163638327!2d76.92339789079226!3d43.248279756730646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836eca1e6f2ed1%3A0xb8591feee8117fb9!2z0YPQuy4g0JrQsNCx0LDQvdCx0LDQuSDQsdCw0YLRi9GA0LAgOTQsINCQ0LvQvNCw0YLRiyAwNTAwMDA!5e0!3m2!1sru!2skz!4v1548305115059" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
<?php require_once('footer.php');?>