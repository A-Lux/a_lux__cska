<?php require_once('header.php');?>

  <main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
        <ul>
          <li><a href="index.php">Главная</a></li>
          <span>/</span>
          <li><a href="#">Экспозиция</a></li>
          <span>/</span>
          <li><a class="bread-crumbs-active" href="#">Книги</a></li>
        </ul>
      </div>
      <div class="middle-title">
        <h3>Книги</h3>
      </div>

      <div class="books-main">
        <ul class="tabs">
          <li class="tab-link current" data-tab="tab-1"> 2018</li>
          <li class="tab-link" data-tab="tab-2">2017</li>
        </ul>

        <div id="tab-1" class="tab-content current">
          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К. 2018</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>
        </div>



        <div id="tab-2" class="tab-content">
          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К. 2017</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>

          <div class="books-item">
            <div class="books-img">
              <img src="images/books.png">
            </div>

            <div class="books-content">
              <p>Автор: <span>Ермеков Ж. К.</span></p>
              <p>Наименование: <span>История Казахстана: Хроники.<br>Энциклопедия. Том 3</span></p>
              <div class="about-book">
                <h5>Описание книги</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>
              </div>
              <a href="books-inner.php" class="more-btn">
                 Подробнее
              </a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="cska-pagination">
        <ul>
          <li><a href="#">В начало</a></li>
          <li><a href="#">Назад</a></li>
          <li><a href="#">1</a></li>
          <li class="active"><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Вперёд</a></li>
          <li><a href="#">В конец</a></li>
        </ul>
      </div>

      <div class="back-main">
        <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
      </div>
  </main>


  <?php require_once('footer.php');?>
