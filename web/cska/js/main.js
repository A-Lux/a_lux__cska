(function($) {

  new WOW().init();

    $(document).ready(function(){
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('+7(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
    });
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
    $(".owl-carousel").owlCarousel({
        nav: true,
        navText: ["<img src='../images/left-arrow.png'>","<img src='../images/right-arrow.png'>"]
    });
   /* OffCanvas Menu
    * ------------------------------------------------------ */
    var clOffCanvas = function() {

        var menuTrigger     = $('.header-menu-toggle'),
            nav             = $('.header-nav'),
            closeButton     = nav.find('.header-nav__close'),
            siteBody        = $('body'),
            mainContents    = $('section, footer');

        // open-close menu by clicking on the menu icon
        menuTrigger.on('click', function(e){
            e.preventDefault();
            // menuTrigger.toggleClass('is-clicked');
            siteBody.toggleClass('menu-is-open');
        });

        // close menu by clicking the close button
        closeButton.on('click', function(e){
            e.preventDefault();
            menuTrigger.trigger('click');
        });

        // close menu clicking outside the menu itself
        siteBody.on('click', function(e){
            if( !$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span') ) {
                // menuTrigger.removeClass('is-clicked');
                siteBody.removeClass('menu-is-open');
            }
        });

    };

   /* Initialize
    * ------------------------------------------------------ */
    (function ssInit() {
        clOffCanvas();
    })();


})(jQuery);
function add() {
    let x;
    x = document.getElementById("width").value;
    x++;
    document.getElementById("width").innerHTML = x;
// END

}

  // TABS STATEs
  $(document).ready(function(){

    $('ul.tabs li').click(function(){
      var tab_id = $(this).attr('data-tab');

      $('ul.tabs li').removeClass('current');
      $('.tab-content').removeClass('current');

      $(this).addClass('current');
      $("#"+tab_id).addClass('current');
    })

  })
  // END TABS STATES

// ONLINE PAY
$(function() {
    $('.online-pay ul').hide();
    $('.online-pay-btn').on('click', function(e) {
        e.preventDefault()
        $('.online-pay ul').slideToggle(500);
        if (!$(this).hasClass('display')) {
            $(this).addClass('display');
            $('.online-pay').css('top', '200px');
            $('.online-pay').css('width', '250px');
        }
        else {
            $('.online-pay').css('top', '350px');
            $('.online-pay').css('width', '');
            $(this).removeClass('display');
        }
    })
})
// END ONLINE PAY


// SF MENU
$(function() {
  
  $('ul.sf-menu').superfish({
      delay:       200,                            // one second delay on mouseout
      animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
      speed:       'fast',                          // faster animation speed
      autoArrows:  false                            // disable generation of arrow mark-up
  });

})
// END SF MENU


// MOBILE MENU
$(function() {
  $('.submenu-head').on('click', function(e) {

    e.preventDefault();
    $(this).parent().find('.mobile-submenu').slideToggle();
});

  $('.mobile-submenu-link').on('click', function(e) {
    e.preventDefault();
    $('.mobile-submenu-sub').slideToggle();
  });

});
// END MOBILE MENU


// SCROLL STYLE
$('.news-block').niceScroll({
  cursorcolor:'#00a3d3'
});
// END SCROLL STYLE


// HAMBURGER MENU BTN
$(function() {
    $('.hamburger-menu-btn').on('click', function(e) {
        e.preventDefault();
        $('.mobile-menu').slideToggle(500);
    })
})
// END HAMBURGER MENU BTN


// TABS
$(function(){
    $('.main-activity-tabs-items').hide();
    $('.main-activity-tabs-items:first').show();
    $('.main-activity-tabs li:first').addClass('tab-active');
    $('.main-activity-tabs li').on('click', function(e) {
        e.preventDefault();
        $('.main-activity-tabs li').removeClass('tab-active');
        $(this).addClass('tab-active');
        $('.main-activity-tabs-items').hide();
        var selectTab = $(this).find('a').attr("href");
        $(selectTab).fadeIn();
    })
})
// END TABS


// CONFERENCE MAIN SLIDER
$(function () {
  
  var conferenceSlide = $('#conference-bottom');
  conferenceSlide.bxSlider({
    pager: false
  });
  
});
// END CONFERENCE MAIN SLIDER

// GALLERY MAGNIFIC POPUP
$(function() {
  $('.gallery-item, .conference-bottom-slide, .conference-top, .pay-feedback').magnificPopup({
    delegate: 'a',
    type: 'image',
    closeOnContentClick: false,
    closeBtnInside: false,
    mainClass: 'mfp-with-zoom mfp-img-mobile',
    gallery: {
      enabled: true
    },
    zoom: {
        enabled: true,
        }
  });
});
// END GALLERY MAGNIFIC POPUP


// GALLERY BLOCK SLIDER
$(function () {
  
  var galleryWrap = $('#gallery-content-wrap');
  galleryWrap.bxSlider({
    pager: false,
    autoDirection: 'next'
  });
  
});
// END GALLERY BLOCK SLIDER




// REPORTS TABLE ACCORDEON

$(document).ready(function() {
  $('.department-accordeon>div').not(':first').hide();
  $('.department-accordeon>a').on('click', function(e) {

    e.preventDefault();

    var findArticle = $(this).next('div');
    var findWrapper = $('.department-accordeon').find('div');

    findWrapper.slideUp();
    findArticle.slideDown();

  });
});



$(function() {

  $('#carousel').waterwheelCarousel(); 

  $('#carousel').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true
  },
  zoom: {
    enabled: true,
}
});

});
// END REPORTS TABLE ACCORDEON