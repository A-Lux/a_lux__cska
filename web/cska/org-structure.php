<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">О центре</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Организационная структура</a></li>
			</ul>
		</div>

	</div>

	<div class="org-structure-wrap">

		<div class="container h-100">

			<div class="org-structure-title">
				<h2>Организационная структура</h2>
			</div>

		</div>

	</div>


	<div class="org-structure-content">

		<div class="container">

			<div class="table-accordeon-wrap">
				<div class="table-accordeon-content">

					<div class="table-administration department-accordeon">
						<a class="table-administration-link" href="#">Администрация</a>

						<div class="table-accordeon">
							<table>
								<tr>
									<th><a href="director-page.php">Директор</a></th> 
									<th><a href="director-page.php">Сулейменов Олжас Омарович</a></th>
								</tr>

								<tr>
									<th>Заместитель директора по науке и внешним связям</th>
									<th>Толмачев Валерий Геннадьевич тел. +7(727)2 612 009</th>
								</tr>

								<tr>
									<th>Заместитель директора по общим и организоционным вопросам</th>
									<th>Сыдыков Айбек Жексеналиевич тел. +7(727)2 613 821</th>
								</tr>

								<tr>
									<th>Главный бухгалтер</th>
									<th>Нуртазина Гульбаршын Кизатулловна тел. +7(727)2 611 692</th>
								</tr>

								<tr>
									<th>Экономист</th>
									<th>Макисменко Вера Александровна тел. +7(727)2 611 692</th>
								</tr>
							</table>
						</div>
					</div>


					<div class="table-department-science department-accordeon">
						<a class="table-department-link" href="#">Отдел науки</a>

						<div class="table-department-content">

							<p>Проведение научно-исследовательских работ  по  историко-культурному  наследию народов  Центральной  Азии  и  его  взаимосвязи  и  историко-культурным  наследием  других регионов  мира. Создание  платформы  диалога  на  основе  научных  достижений  для  укрепления  межэтнических  и  межконфессиональных  отношений</p>

							<h4>Руководитель отдела:</h4>
							<span>+7(727)2 611 104</span>

						</div>
					</div>


					<div class="table-department-external department-accordeon">
						<a class="table-department-link" href="#">Отдел внешних связей</a>

						<div class="table-department-content">

								<p>Содействие   международному  сотрудничеству  в  сфере  культуры  науки  и  образования. Развития культурных и образовательных  контактов  между странами  региона</p>

								<h4>Руководитель отдела: Новоженов Виктор Александрович</h4>
								<span>тел. +7(727)2 611 104</span>

						</div>
					</div>


					<div class="table-department-conf department-accordeon">
						<a class="table-department-link" href="#">Отдел выставок и конференции</a>

						<div class="table-department-content">

								<p>Пропаганда  историко-культурного  наследия  народов  Центральной  Азии  и его  взаимосвязи  с  историко-культурным  наследием других регионов  мира. Создание  научно-информационной  базы данных  по культурному  разнобразию  стран  Центральной  Азии  и  взаимодействию ее  культур  с культурами  других регионами  мира</p>

								<h4>Руководитель отдела: Нурпеисов Мурат Мугалович</h4>
								<span>тел. +7(727)2 611 104</span>

						</div>
					</div>

				</div>

				<div class="back-main">
					<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
				</div>

			</div>

		</div>

	</div>
		<!-- END MAIN -->

		<?php require_once('footer.php');?>