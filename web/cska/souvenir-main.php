<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">Экспозиция</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Сувенирная продукция</a></li>
			</ul>
		</div>

	</div>

		<div class="container">

			<div class="souvenir-main-wrap">
				<div class="souvenir-main-title">
					<h2>Сувенирная продукция</h2>
				</div>

				<div class="souvenir-content">

					
					<div class="souvenir-main-products">
						
						<!-- 1 СТРОКА -->
						<div class="souvenir-products-content">

							<div class="souvenir-products-left">
								<img src="images/cup.png" alt="Кружка">
							</div>

							<div class="souvenir-products-right">
								<h4>Сувенирная кружка</h4>

								<h5>Описание книги</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>

								<a href="souvenir-inner.php" class="souvenir-products-btn">Подробнее</a>
							</div>

						</div>
						<!-- END 1 СТРОКА -->



						<!-- 2 СТРОКА -->
						<div class="souvenir-products-content">

							<div class="souvenir-products-left">
								<img src="images/souvenir-notebook.png" alt="Кружка">
							</div>

							<div class="souvenir-products-right">
								<h4>Сувенирнйы блокнот</h4>

								<h5>Описание книги</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>

								<a href="souvenir-inner.php" class="souvenir-products-btn">Подробнее</a>
							</div>

						</div>
						<!-- END 2 СТРОКА -->



						<!-- 3 СТРОКА -->
						<div class="souvenir-products-content">

							<div class="souvenir-products-left">
								<img src="images/cup.png" alt="Кружка">
							</div>

							<div class="souvenir-products-right">
								<h4>Сувенирнйы блокнот</h4>

								<h5>Описание книги</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>

								<a href="souvenir-inner.php" class="souvenir-products-btn">Подробнее</a>
							</div>

						</div>
						<!-- END 3 СТРОКА -->



						<!-- 4 СТРОКА -->
						<div class="souvenir-products-content">

							<div class="souvenir-products-left">
								<img src="images/souvenir-notebook.png" alt="Кружка">
							</div>

							<div class="souvenir-products-right">
								<h4>Сувенирнйы блокнот</h4>

								<h5>Описание книги</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco pariatur.</p>

								<a href="souvenir-inner.php" class="souvenir-products-btn">Подробнее</a>
							</div>

						</div>
						<!-- END 4 СТРОКА -->

					</div>
					

				</div>

			</div>

			<div class="cska-pagination">
				<ul>
					<li><a href="#">В начало</a></li>
					<li><a href="#">Назад</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">Вперёд</a></li>
					<li><a href="#">В конец</a></li>
				</ul>
			</div>

		<div class="back-main">
			<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
		</div>

	</div>

	<!-- END MAIN -->

	<?php require_once('footer.php');?>