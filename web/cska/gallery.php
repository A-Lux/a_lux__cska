<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">Медиа</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Галерея</a></li>
			</ul>
		</div>

	</div>

	<div class="gallery-wrap">

		<div class="container">

			<div class="gallery-title">
				<h2>Галерея</h2>
			</div>
	
			<div class="gallery-content-wrap" id="gallery-content-wrap">
				<div class="gallery-content" id="gallery-content">
					<!-- 1 СТРОКА -->
					<div class="gallery-item">
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
					</div>
					<!-- END 1 СТРОКА -->


					<!-- 2 СТРОКА -->
					<div class="gallery-item mb-45">
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
					</div>
					<!-- END 2 СТРОКА -->
				</div>

				<div class="gallery-content" id="gallery-content">
					<!-- 1 СТРОКА -->
					<div class="gallery-item">
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
					</div>
					<!-- END 1 СТРОКА -->

					<!-- 2 СТРОКА -->
					<div class="gallery-item mb-45">
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
						<a href="images/gallery-image.jpg"><span class="gallery-zoom"><img src="images/zoom.png" alt="zoom"></span><img src="images/gallery-image.jpg" alt="gallery image"></a>
					</div>
					<!-- END 2 СТРОКА -->
				</div>
			</div>

			<div class="cska-pagination">
				<ul>
					<li><a href="#">В начало</a></li>
					<li><a href="#">Назад</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">Вперёд</a></li>
					<li><a href="#">В конец</a></li>
				</ul>
			</div>

			<div class="back-main">
				<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
			</div>

		</div>

	</div>

	<!-- END MAIN -->

	<?php require_once('footer.php');?>