<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">Экспозиция</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Прейскурант</a></li>
			</ul>
		</div>

	</div>

	<div class="container">
		
		<div class="price-list-wrap">
			<div class="price-list-title">
				<h2>Прейскурант</h2>
				<p>«Мәдениеттерді жақындастыру орталығы» мемлекеттік музейі»РМҚК өндіретін және өткізетін тауарлардың (атқаратын  жұмыстардың,көрсететін қызметтердің) ақылы қызмет көрсетудің баға тізбесі</p>
			</div>

			<div class="price-list-content">
				
				<table class="table-responsive">
					<!-- СТРОКА -->
					<tr>
						<th>№</th>
						<th>Ақылы қызмет көрсету атаулары</th>
						<th>Өлшем бірлігі</th>
						<th>Өлшем бірлігінің теңгегешаққандағы құны</th>
					</tr>
					<!-- END СТРОКА -->
	
					<!-- 1 СТРОКА -->
					<tr>
						<td>1</td>
						<td><h6>Музейге жеке келушілер:</h6>
								<ul>
									<li><span>ересектерге;</span></li>
									<li><span>студенттер мен зейнеткерлерге;</span></li>
									<li><span>оқушыларға;</span></li>
									<li><span>мектеп жасына дейінгі балаларға</span></li>
								</ul>
						</td>
						<td>1 адамға</td>
						<td><strong>200 <br> 150 <br> 100 <br> ақысыз</strong></td>
					</tr>
					<!-- 1 СТРОКА -->

					<!-- 2 СТРОКА -->
					<tr>
						<td>2</td>
						<td><h6>Ұжымдық келушілерге:</h6>
								экскурсиялық қызмет көрсету (топта - 10 адам):
								<ul>
									<li><span>қазақ және орыс тілдерінде;</span></li>
									<li><span>шет тілдерінде;</span></li>
									<li><span>оқушыларға және мектеп жасына дейінгі балаларға қазақ, орыс және шет тілдерінде.</span></li>
								</ul>
						</td>
						<td>1 академиялық сағат</td>
						<td><strong>600 <br> 1000 <br> 400</strong></td>
					</tr>
					<!-- END 2 СТРОКА -->


					<!-- 3 CТРОКА -->
					<tr>
						<td>3</td>
						<td><h6>Тақырыптық экскурсия:</h6>
								экскурсиялық қызмет көрсету (топта - 10 адам):
								<ul>
									<li><span>ересектерге;</span></li>
									<li><span>оқушылар мен студенттерге;</span></li>
								</ul>
						</td>
						<td>1 академиялық сағат</td>
						<td><strong>900 <br> 650</strong></td>
					</tr>
					<!-- END 3 CТРОКА -->


					<!-- 4 CТРОКА -->
						<tr>
							<td>4</td>
							<td><h6>Фото түсірілімдер жүргізу:</h6>
									экскурсиялық қызмет көрсету (топта - 10 адам):
									<ul>
										<li><span>музей жәдігерлерін суретке түсіру;</span></li>
										<li><span>музей жәдігерлерімен бірге суретке түсу.</span></li>
									</ul>
							</td>
							<td>1 данасы</td>
							<td><strong>300 – 1 500 <br> 500</strong></td>
						</tr>
						<!-- END 4 CТРОКА -->
				

					<!-- 5 CТРОКА -->
					<tr>
						<td>5</td>
						<td><h6>Полиграфия өнімдерін өткізу:</h6>
								экскурсиялық қызмет көрсету (топта - 10 адам):
								<ul>
									<li><span>күнтізбе;</span></li>
									<li><span>плакат;</span></li>
									<li><span>буклет;</span></li>
									<li><span>блокнот;</span></li>
									<li><span>кітап;</span></li>
									<li><span>жолнұсқаулық;</span></li>
								</ul>
						</td>
						<td>1 данасы</td>
						<td><strong>500 – 2 000 <br> 700 – 2 200 <br> 700 – 2 500 <br> 500 – 1 000 <br> 1 000 – 5 000 <br> 500 – 2 000</strong></td>
					</tr>
					<!-- END 5 CТРОКА -->

					<!-- 6 CТРОКА -->
					<tr>
						<td>6</td>
						<td><h6>Кәдесыйлар</h6>
						</td>
						<td>1 данасы</td>
						<td><strong>500 - 5000</strong></td>
					</tr>
					<!-- END 6 CТРОКА -->


					<!-- 7 CТРОКА -->
					<tr>
						<td>7</td>
						<td><h6>Мұражай жинақтарынан көрме өткізу</h6>
						</td>
						<td>1 академиялық сағат</td>
						<td><strong>500 – 2 000</strong></td>
					</tr>
					<!-- END 7 CТРОКА -->
				</table>

			</div>

		</div>

		<div class="back-main">
			<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
		</div>

	</div>


		<!-- END MAIN -->

		<?php require_once('footer.php');?>