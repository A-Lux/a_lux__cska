<?php require_once('header.php');?>


    <!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="index.php">Главная</a></li>
                    <span>/</span>
                    <li><a href="#">Контакты</a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="#">Блог директора</a></li>
                </ul>
            </div>

        </div>
        <div class="gallery-title">
            <h2>Блог директора</h2>
        </div>

        <div class="container">
            <div class="mandat-content">
                <img src="images/director-img.jpg" class="img-fluid " alt="">
                <p>The standard Lorem Ipsum passage, used since the 1500s</p>
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit </p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit </p>
                <p>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                </p>
                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>

                <div class="contacts-btn no-top">
                  <a href="contacts.php" class="blog-btn">Обратная связь</a>
                </div>
                
            </div>
            <div class="back-main space-bot">
                <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
            </div>
        </div>

    </div>
    <!-- END MAIN -->

<?php require_once('footer.php');?>