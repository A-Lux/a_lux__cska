<?php require_once('header.php');?>



<main class="main">
  <div class="container">
    <div class="bread-crumbs wow fadeInLeft">
      <ul>
        <li><a href="#">Главная</a></li>
        <span>/</span>
        <li><a href="#">Медиа</a></li>
        <span>/</span>
        <li><a class="bread-crumbs-active" href="#">Пресс релизы</a></li>
      </ul>
    </div>

    <div class="press-releases">

      <div class="container">

        <div class="row">

          <div class="col-12">
            <div class="press-title">
              <h1>Пресс - релизы</h1>
            </div>
          </div>

        </div>

        <!-- 1 -->
        <div class="press-content">
          <div class="row">

            <div class="col-lg-3">
              <div class="press-left">
                <img src="images/men.jpg" alt="men">
              </div>
            </div>

            <div class="col-lg-9">
              <div class="press-right">
                <h3>«Государственные символы – светлый  объединяющий  образ Независимости»</h3>

                <h6>Описание</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ...</p>

                <a href="press-inner.php" class="press-btn">Подробнее</a>
              </div>
            </div>

          </div>

        </div>
        <!-- end 1 -->


        <!-- 2 -->
        <div class="press-content">
          <div class="row">

            <div class="col-lg-3">
              <div class="press-left">
                <img src="images/men.jpg" alt="men">
              </div>
            </div>

            <div class="col-lg-9">
              <div class="press-right">
                <h3>«Государственные символы – светлый  объединяющий  образ Независимости»</h3>

                <h6>Описание</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ...</p>

                <a href="press-inner.php" class="press-btn">Подробнее</a>
              </div>
            </div>

          </div>

        </div>
        <!-- end 2 -->


        <!-- 3 -->
        <div class="press-content">
          <div class="row">

            <div class="col-lg-3">
              <div class="press-left">
                <img src="images/men.jpg" alt="men">
              </div>
            </div>

            <div class="col-lg-9">
              <div class="press-right">
                <h3>«Государственные символы – светлый  объединяющий  образ Независимости»</h3>

                <h6>Описание</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ...</p>

                <a href="press-inner.php" class="press-btn">Подробнее</a>
              </div>
            </div>

          </div>

        </div>
        <!-- end 3 -->


      </div>
      

    </div>

  </div>

  <div class="cska-pagination">
    <ul>
      <li><a href="#">В начало</a></li>
      <li><a href="#">Назад</a></li>
      <li><a href="#">1</a></li>
      <li class="active"><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">Вперёд</a></li>
      <li><a href="#">В конец</a></li>
    </ul>
  </div>

  <a href="#" class="return-main-page">
    Вернуться на главную
  </a>
</div>
</main>


<?php require_once('footer.php');?>
