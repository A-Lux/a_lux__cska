<?php require_once('header.php');?>

<!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="index.php">Главная</a></li>
                    <span>/</span>
                    <li><a href="#">Экспозиция</a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="#">Книги</a></li>
                </ul>
            </div>

        </div>

        <div class="container">

            <div class="books-inner-wrap">

                <div class="books-inner-title">
                    <h2>Книги</h2>
                </div>

                <div class="books-inner-content">

                    <div class="books-inner-left">
                        <img src="images/book-kz.jpg" alt="История Казахстана">
                    </div>

                    <div class="books-inner-right">
                        <h5>Автор</h5>
                        <h6>Ермеков Ж. К.</h6>

                        <h5>Название</h5>
                        <h6>История Казахстана: Хроники. Энциклопедия. Том 3</h6>

                        <h5 style="color: #000">Описание книги</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. se cillum dolore eu fugiat nulla pariatur. </p>

                        <h3>Год выпуска: 2016</h3>
                    </div>

                </div>

                <div class="pay-books">

                    <h3>Покупка книги</h3>

                    <div class="pay-books-input">

                        <div class="pay-books-input-item">
                            <h6>Ваш E-mail адрес:*</h6>
                            <input type="text"> <span>На него будет отправлен ваш билет и инструкция</span>
                        </div>

                        <div class="pay-books-input-item">
                            <h6>Ваш номер мобильного телефона:*</h6>
                            <input class="phone_us" placeholder="+7 (777) 489 36 54 " type="text"> <span>На него будет отправлен ваш билет и инструкция</span>
                        </div>

                        <input id="pay-books-agree" type="checkbox">
                        <label for="pay-books-agree">Я согласен с условиями <a href="">соглашения</a></label>

                        <div class="check-books-form-pay">
                            <div class="check-books-form-pay-left">
                                <h6>Выберите форму оплаты:</h6>
                            </div>

                            <div class="check-books-form-pay-right">
                                <form action="">
                                    <select>
                                        <option>Казахстанские карты Visa/MasterCard</option>
                                        <option>Казахстанские карты Visa/MasterCard</option>
                                        <option>Казахстанские карты Visa/MasterCard</option>
                                        <option>Казахстанские карты Visa/MasterCard</option>
                                    </select>

                                    <button>Отправить</button>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="cska-pagination">
                <ul>
                    <li><a href="#">В начало</a></li>
                    <li><a href="#">Назад</a></li>
                    <li><a href="#">1</a></li>
                    <li class="active"><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Вперёд</a></li>
                    <li><a href="#">В конец</a></li>
                </ul>
            </div>

            <div class="back-main">
                <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
            </div>

        </div>

        <!-- END MAIN -->



<?php require_once('footer.php');?>