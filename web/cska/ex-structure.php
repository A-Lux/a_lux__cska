<?php require_once('header.php');?>

<!-- MAIN -->
<div class="main">

	<div class="container">

		<div class="bread-crumbs wow fadeInLeft">
			<ul>
				<li><a href="index.php">Главная</a></li>
				<span>/</span>
				<li><a href="#">Экспозиция</a></li>
				<span>/</span>
				<li><a class="bread-crumbs-active" href="#">Структура</a></li>
			</ul>
		</div>

	</div>

	<div class="ex-structure-wrap">

		<div class="container h-100">

			<div class="ex-structure-title">
				<h2>Структура</h2>
			</div>

		</div>

	</div>


	<div class="ex-structure-content">

		<div class="container">

			<div class="table-accordeon-wrap">
				<div class="table-accordeon-content">

					<div class="table-administration department-accordeon">
						<a class="table-administration-link" href="#">Административный департамент</a>

						<div class="table-accordeon">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>


					<div class="table-department-science department-accordeon">
						<a class="table-department-link" href="#">Департамент по делам культуры и искусства</a>

						<div class="table-department-content">

							<p>Проведение научно-исследовательских работ  по  историко-культурному  наследию народов  Центральной  Азии  и  его  взаимосвязи  и  историко-культурным  наследием  других регионов  мира. Создание  платформы  диалога  на  основе  научных  достижений  для  укрепления  межэтнических  и  межконфессиональных  отношений</p>

							<h4>Руководитель отдела:</h4>
							<span>+7(727)2 611 104</span>

						</div>
					</div>


					<div class="table-department-external department-accordeon">
						<a class="table-department-link" href="#">Департамент архивного дела и документации</a>

						<div class="table-department-content">

								<p>Содействие   международному  сотрудничеству  в  сфере  культуры  науки  и  образования. Развития культурных и образовательных  контактов  между странами  региона</p>

								<h4>Руководитель отдела: Новоженов Виктор Александрович</h4>
								<span>тел. +7(727)2 611 104</span>

						</div>
					</div>


					<div class="table-department-conf department-accordeon">
						<a class="table-department-link" href="#">Департамент анализа и стратегического планирования</a>

						<div class="table-department-content">

								<p>Пропаганда  историко-культурного  наследия  народов  Центральной  Азии  и его  взаимосвязи  с  историко-культурным  наследием других регионов  мира. Создание  научно-информационной  базы данных  по культурному  разнобразию  стран  Центральной  Азии  и  взаимодействию ее  культур  с культурами  других регионами  мира</p>

								<h4>Руководитель отдела: Нурпеисов Мурат Мугалович</h4>
								<span>тел. +7(727)2 611 104</span>

						</div>
					</div>
					
					<div class="table-department-conf department-accordeon">
						<a class="table-department-link" href="#">Департамент экономики и финансов</a>

						<div class="table-department-content">

								<p>Пропаганда  историко-культурного  наследия  народов  Центральной  Азии  и его  взаимосвязи  с  историко-культурным  наследием других регионов  мира. Создание  научно-информационной  базы данных  по культурному  разнобразию  стран  Центральной  Азии  и  взаимодействию ее  культур  с культурами  других регионами  мира</p>

								<h4>Руководитель отдела: Нурпеисов Мурат Мугалович</h4>
								<span>тел. +7(727)2 611 104</span>

						</div>
					</div>

				</div>

				<div class="back-main">
					<a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
				</div>

			</div>

		</div>

	</div>
		<!-- END MAIN -->

		<?php require_once('footer.php');?>