  <?php require_once('header.php');?>

  <main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
        <ul>
          <li><a href="index.php">Главная</a></li>
          <span>/</span>
          <li><a href="#">Мероприятия/Проекты</a></li>
          <span>/</span>
          <li><a class="bread-crumbs-active" href="#">Грантовые проекты</a></li>
        </ul>
      </div>
      <div class="middle-title">
        <h3>Грантовые проекты</h3>
      </div>

      <div class="grant-projects">
        <div class="project-item">
          <img src="images/project1.png">
          <div class="project-content">
            <a href="grant-projects-inner-1.php">Проект 1</a>
            <p>«Великий  Шелковый путь: города как центры сближения духовной культуры, конфессий и хранители сакральныхтрадиций»</p>
          </div>
        </div>

        <div class="project-item">
          <img src="images/project1.png">
          <div class="project-content">
            <a href="grant-projects-inner-2.php">Проект 2</a>
            <p>«Разработка модели коммуникацийнаселения Центральной Азии в древности и средневековье: взаимодействие традиций и диалог культур»</p>
          </div>
        </div>
      </div>

      <a href="#" class="return-main-page">
        Вернуться на главную
      </a>
    </div>
  </main>

  <?php require_once('footer.php');?>
