	<!-- FOOTER -->
	<footer class="footer" id="footer">

		<div class="footer-top-wrap wow slideInUp">
			<div class="container">

				<div class="footer-top">
					<ul class="footer-links">
						<li><a href="#">Главная</a></li>
						<li><a href="#">О Центре</a></li>
						<li><a href="#">Публикации</a></li>
						<li><a href="#">3D тур</a></li>
					</ul>

					<ul class="footer-links">
						<li><a href="#">Онлайн–билет</a></li>
						<li><a href="#">Медиа</a></li>
						<li><a href="#">Новости</a></li>
						<li><a href="#">Контакты </a></li>
					</ul>

					<ul class="footer-links">
						<li><a href="#">Выставки</a></li>
						<li><a href="#">Конференции</a></li>
						<li><a href="#">Приобретение билетов</a></li>
						<li><a href="#">Бронирование экскурсий в музей</a></li>
					</ul>

					<ul class="footer-links">
						<li><a href="#">Покупка книг Центра и сувенирной продукции</a></li>
						<li><a href="#">Госзакупки</a></li>
						<li><a href="#">Новости министерства культуры и спорта РК</a></li>
						<li><a href="#">Обратная связь</a></li>
					</ul>
					
					<div class="footer-contacts">
						<div class="footer-phone">
							<img src="images/footer-phone.png" alt="Телефон">
							<span>(727) 261-20-09  (727) 261-16-92</span>
						</div>

						<div class="footer-message">
							<img src="images/footer-message.png" alt="Сообщение">
							<span>muzei@mail.ru</span>
						</div>

						<div class="footer-location">
							<img src="images/footer-location.png" alt="Локация">
							<span>Алматы л.Кабанбай батыра, 94</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom-wrap wow slideInUp">

			<div class="container">
				<div class="footer-bottom">
					<div class="footer-bottom-copyright">
						<p>© 2018 Республиканское Государственное Казенное Предприятие «Государственный музей. Центр сближения культур» Министерства культуры и спорта Республики Казахстан. Все права защищены</p>
					</div>

					<div class="developed-by">
						<a target="_blank" href="https://www.a-lux.kz/"><img src="images/logo_a-lux.png" alt="Разработано в веб студии A-Lux"></a>
						<span>Разработано <br> компанией A-lux</span>
					</div>

					<div class="footer-bottom-social">
						<a href="#"><img src="images/facebook.png" alt="facebook"></a>
						<a href="#"><img src="images/vk.png" alt="vk"></a>
						<a href="#"><img src="images/instagram.png" alt="instagram"></a>
						<a href="#"><img src="images/twitter.png" alt="twitter"></a>
						<a href="#"><img src="images/google.png" alt="google"></a>
					</div>
				</div>
			</div>

		</div>

	</footer>
	<!-- END FOOTER -->

</body>
</html>