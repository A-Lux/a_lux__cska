<?php require_once('header.php');?>


    <!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="partners.php">Партнеры</a></li>
                </ul>
            </div>

        </div>
        <div class="gallery-title">
            <h2>Партнеры</h2>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner3.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner5.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner6.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner3.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner5.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner6.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner3.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner5.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner6.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner2.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner3.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner1.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner5.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href=""><img src="images/partner6.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="cska-pagination">
                        <ul>
                            <li><a href="#">В начало</a></li>
                            <li><a href="#">Назад</a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">Вперёд</a></li>
                            <li><a href="#">В конец</a></li>
                        </ul>
                    </div>
                </div>
                <div class="back-main space-bot">
                    <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
                </div>
            </div>
        </div>

    </div>
    <!-- END MAIN -->

<?php require_once('footer.php');?>