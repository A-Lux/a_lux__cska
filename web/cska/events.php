<?php require_once('header.php');?>


    <!-- MAIN -->
    <div class="main">

        <div class="container">

            <div class="bread-crumbs wow fadeInLeft">
                <ul>
                    <li><a href="index.php">Главная</a></li>
                    <span>/</span>
                    <li><a href="#">Новости</a></li>
                    <span>/</span>
                    <li><a class="bread-crumbs-active" href="#">События</a></li>
                </ul>
            </div>

        </div>
        <div class="gallery-title">
            <h2>События</h2>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="news-block">
                        <div class="news-block-item">
                            <a href=""><p>Развитие человеческих
                                    ресурсов путем подготовки </p></a>
                            <div class="news-date">
                                <p>24 февраля 2019</p>
                            </div>
                        </div>
                        <div class="news-block-item">
                            <a href=""><p>Развитие человеческих
                                    ресурсов путем подготовки </p></a>
                            <div class="news-date">
                                <p>24 февраля 2019</p>
                            </div>
                        </div>
                        <div class="news-block-item">
                            <a href=""><p>Развитие человеческих
                                    ресурсов путем подготовки </p></a>
                            <div class="news-date">
                                <p>24 февраля 2019</p>
                            </div>
                        </div>
                        <div class="news-block-item">
                            <a href=""><p>Развитие человеческих
                                    ресурсов путем подготовки </p></a>
                            <div class="news-date">
                                <p>24 февраля 2019</p>
                            </div>
                        </div>
                        <div class="news-block-item">
                            <a href=""><p>Развитие человеческих
                                    ресурсов путем подготовки </p></a>
                            <div class="news-date">
                                <p>24 февраля 2019</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="">
                                    <img src="images/news-inner.jpg" alt="">
                                    <p>Развитие человеческих
                                        ресурсов путем подготовки </p>
                                    <div class="news-date">
                                        <p>24 февраля 2019</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="cska-pagination">
                        <ul>
                            <li><a href="#">В начало</a></li>
                            <li><a href="#">Назад</a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">Вперёд</a></li>
                            <li><a href="#">В конец</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="back-main space-bot">
                <a href="index.php"><img src="images/mandat-arrow.png" alt="arrow">Вернуться на главную</a>
            </div>
        </div>

    </div>
    <!-- END MAIN -->

<?php require_once('footer.php');?>