<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.02.2019
 * Time: 17:08
 */

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "public/css/superfish.css",
        "public/css/bootstrap.css",
        "public/css/jquery.bxslider.css",
        "public/css/owl.carousel.min.css",
        "public/css/owl.theme.default.min.css",
        "public/css/slick-theme.css",
        "public/css/slick.css",
        "public/css/animate.css",
        "public/css/style.css",
        "public/css/responsive.css",
        "public/css/jquery.hislide.css",
        "public/css/magnific-popup.css",
        "public/css/cascade.css",
        "https://fonts.googleapis.com/css?family=Lobster",

    ];
    public $js = [
           "public/js/jquery-3.2.1.min.js",
           "public/js/superfish.min.js",
           "public/js/wow.min.js",
           "public/js/jquery.mask.min.js",
           "public/js/cascade-slider.js",
           "public/js/prefixfree.min.js",
           "public/js/jquery.bxslider.min.js",
           "public/js/owl.carousel.min.js",
           "public/js/jquery.waterwheelCarousel.min.js",
           "public/js/jquery.nicescroll.min.js",
           "public/js/jquery.hislide",
           "public/js/slick.min.js",
           "public/js/main.js",
           "public/js/jquery.magnific-popup.min.js",
           "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
           "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
    ];
    public $depends = [

    ];
}