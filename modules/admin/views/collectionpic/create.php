<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Collectionpic */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Картинки коллекции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collectionpic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
