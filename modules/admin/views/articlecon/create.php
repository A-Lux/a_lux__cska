<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Articlecon */

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Содержании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articlecon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
