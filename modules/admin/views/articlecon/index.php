<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleconSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Содержании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articlecon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],

            ['attribute'=>'articlesub_id', 'value'=>function($model){ return $model->contentName;},'filter'=>\app\models\Articlesub::getList()],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
