<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conferencepic */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Картинки конференции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conferencepic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
