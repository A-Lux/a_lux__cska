<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directorblog */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Блог директора', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="directorblog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
