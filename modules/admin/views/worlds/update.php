<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Worlds */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'В мире', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="worlds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
