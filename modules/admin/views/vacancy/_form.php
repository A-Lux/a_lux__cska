<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?php
	if(Yii::$app->session['lang'] == ''){
		$emp = array('Полная занятость'=>'Полная занятость','Частичная занятость'=>'Частичная занятость','Стажировка'=>'Стажировка',
        'Проектная/Временная работа'=>'Проектная/Временная работа','Волонтерство'=>'Волонтерство');
	}elseif(Yii::$app->session['lang'] == '_kz'){
    	$emp = array('Полная занятость'=>'Полная занятость','Частичная занятость'=>'Частичная занятость','Стажировка'=>'Стажировка',
        'Проектная/Временная работа'=>'Проектная/Временная работа','Волонтерство'=>'Волонтерство');
	}elseif(Yii::$app->session['lang'] == '_en'){
    	$emp = array('Full time'=>'Full time','Part-time'=>'Part-time','Internship'=>'Internship',
        'Project / Temporary Work'=>'Project / Temporary Work','Volunteering'=>'Volunteering');
	}
    ?>

    <?= $form->field($model, 'employment')->dropDownList($emp) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'date')-> ?>


    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
