<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mainslidea */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Содержание', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="mainslidea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
