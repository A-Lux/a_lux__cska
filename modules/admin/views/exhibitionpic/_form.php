<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Exhibitionpic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exhibitionpic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image[]')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
    ]);
    ?>

    <?= $form->field($model, 'exhibition_id')->dropDownList(\app\models\Exhibition::getList()) ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
