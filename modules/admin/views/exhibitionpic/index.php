<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExhibitionpicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Картинки выставки ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exhibitionpic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cоздать картинка выставки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],

            ['attribute'=>'exhibition_id', 'value'=>function($model){ return $model->contentName;},'filter'=>\app\models\Exhibition::getList()],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
