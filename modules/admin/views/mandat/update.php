<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mandat */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Задача, цели и мандат', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="mandat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
