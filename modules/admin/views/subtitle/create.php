<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subtitle */

$this->title = 'Создание подкатегории страницы';
$this->params['breadcrumbs'][] = ['label' => 'Страницы (заголовки и баннеры)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subtitle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
