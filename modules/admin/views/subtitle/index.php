<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubtitleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Страницы (заголовки и баннеры)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subtitle-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function($data){
                    return $data->title;
                }
            ],
//            [
//                'format' => 'html',
//                'attribute' => 'image',
//                'value' => function($data){
//                    return Html::img($data->getImage(), ['width'=>200]);
//                }
//            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
        ],
    ]); ?>
</div>
