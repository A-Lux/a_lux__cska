<?php

use kartik\date\DatePicker;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Eventcon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventcon-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?php

    echo DatePicker::widget([
        'name'=>'Eventcon[date]',
        'value' => $model->date,
        'options' => ['placeholder' => 'Выберите дату...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]);

    ?>


    <?php

    echo $form->field($model, 'content')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]);
    ?>

    <?= $form->field($model, 'eventtypes_id')->dropDownList(\app\models\Eventtypes::getList()) ?>

    <?= $form->field($model, 'recommend')->dropDownList([0 => 'Не рекомендовано', 1 => 'Рекомендовано']) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

<!--    --><?//= $form->field($model, 'date')->textInput() ?>



    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
