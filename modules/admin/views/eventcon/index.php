<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventconSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Содержания';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventcon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать содержания', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'text',
            [
                'format' => 'html',
                'attribute' => 'date',
                'value' => function($data){
                    return $data->getDate();
                }
            ],

            ['attribute'=>'eventtypes_id', 'value'=>function($model){ return $model->eventName;},'filter'=>\app\models\Eventtypes::getList()],
//            'content:ntext',
//            'image',
            [
                'attribute' => 'recommend',
                'filter' => \app\modules\admin\controllers\LabelRec::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelRec::statusLabel($model->recommend);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
</div>
