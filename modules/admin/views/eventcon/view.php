<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Eventcon */

$this->title = $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Заголовок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="eventcon-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            [
                'format' => 'html',
                'attribute' => 'date',
                'value' => function($data){
                    return $data->getDate();
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],

            ['attribute'=>'eventtypes_id', 'value'=>function($model){ return $model->eventName;}],

            [
                'attribute' => 'recommend',
                'filter' => \app\modules\admin\controllers\LabelRec::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelRec::statusLabel($model->recommend);
                },
                'format' => 'raw',
            ],
            [

                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>400]);
                }
            ],
        ],
    ]) ?>

</div>
