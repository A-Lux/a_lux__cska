<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pricelist */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Содержания прейскуранта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pricelist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'contenta',
                'value' => function($data){
                    return $data->contenta;
                }
            ],

            'unit',

            [
                'format' => 'raw',
                'attribute' => 'contentb',
                'value' => function($data){
                    return $data->contentb;
                }
            ],
        ],
    ]) ?>

</div>
