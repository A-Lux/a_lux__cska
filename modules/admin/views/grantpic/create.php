<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grantpic */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Картинки проекта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grantpic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
