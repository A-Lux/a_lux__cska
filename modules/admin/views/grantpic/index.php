<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GrantpicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Картинки проекта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grantpic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать картинка проекта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],

            ['attribute'=>'grant_id', 'value'=>function($model){ return $model->contentName;},'filter'=>\app\models\Grant::getList()],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
