<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Содержании книгов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookin-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'author',

//            'content:ntext',
//            'image',
            ['attribute'=>'booksub_id', 'value'=>function($model){ return $model->contentName;},'filter'=>\app\models\Booksub::getList()],



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
