<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bookin */

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Содержании книгов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
