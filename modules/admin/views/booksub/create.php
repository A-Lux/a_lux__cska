<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Booksub */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Подкатегории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booksub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
