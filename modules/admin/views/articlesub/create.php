<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Articlesub */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Подкатегории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articlesub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
