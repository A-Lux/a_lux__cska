<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Strucadmin */

$this->title = 'Редактировании сотрудников: ' . $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Strucadmins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="strucadmin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
