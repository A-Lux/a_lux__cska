<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Slideright */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Правый слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slideright-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
