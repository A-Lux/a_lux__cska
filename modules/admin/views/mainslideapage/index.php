<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MainslideapageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Пункты";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mainslideapage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'text',
            'url',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
