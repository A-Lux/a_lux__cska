<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

$this->title = $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Отчеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>



    <?php $filelink = $model->getFile();?>
    <?php if($filelink != null) $file = ['attribute'=>'file','value' => Html::a('Посмотреть файл',$filelink,['target'=>'_blank']),'format' => 'raw' ];
    else $file = "file"?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            $file,
        ],
    ]) ?>

</div>
