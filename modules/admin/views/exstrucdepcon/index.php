<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExstrucdepconSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Содержании департаментов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exstrucdepcon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'position',
//            'phone',
//            'content:ntext',

            ['attribute'=>'exstrucdepsub_id', 'value'=>function($model){ return $model->subName;},'filter'=>\app\models\Exstrucdepsub::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
