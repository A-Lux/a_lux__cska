<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grant */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Пресс релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
