<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pricelistsub */

$this->title = 'Названия колонка';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pricelistsub-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'format' => 'raw',
                'attribute' => 'contenta',
                'value' => function($data){
                    return $data->contenta;
                }
            ],

            'unit',

            [
                'format' => 'raw',
                'attribute' => 'contentb',
                'value' => function($data){
                    return $data->contentb;
                }
            ],
        ],
    ]) ?>

</div>
