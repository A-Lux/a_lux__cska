<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pricelistsub */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Названия колонка', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="pricelistsub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
