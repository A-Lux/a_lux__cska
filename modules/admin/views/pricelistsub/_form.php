<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pricelistsub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pricelistsub-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contenta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contentb')->textInput(['maxlength' => true]) ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
