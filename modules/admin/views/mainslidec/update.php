<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mainslidec */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Билеты,конференции,экскурсии', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="mainslidec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
