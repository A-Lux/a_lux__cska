<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StrucconSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Отделы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="struccon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать отдел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'position',

            ['attribute'=>'strucsub_id', 'value'=>function($model){ return $model->subName;},'filter'=>\app\models\Strucsub::getList()],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
