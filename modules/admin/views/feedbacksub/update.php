<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Feedbacksub */

$this->title = 'Редактиование ';
$this->params['breadcrumbs'][] = ['label' => 'Подзаголовок', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактиование';
?>
<div class="feedbacksub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
