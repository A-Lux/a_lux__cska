<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exhibition */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Выставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exhibition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
