<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Eventsub */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Заголовок', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="eventsub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
