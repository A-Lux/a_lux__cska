<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Souvenir */

$this->title = 'Создания сувенирная продукция';
$this->params['breadcrumbs'][] = ['label' => 'Souvenirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="souvenir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
