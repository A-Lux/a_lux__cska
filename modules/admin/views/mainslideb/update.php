<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mainslideb */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Второй блок', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="mainslideb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
