<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Presspic */

$this->title = 'Редактирвания картинка: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Картинки пресс релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирвание';
?>
<div class="presspic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
