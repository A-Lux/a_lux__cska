<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Presspic */

$this->title = 'Создания';
$this->params['breadcrumbs'][] = ['label' => 'Картинки пресс релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presspic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
