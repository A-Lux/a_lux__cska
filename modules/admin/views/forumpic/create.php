<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Forumpic */

$this->title = 'Создания картинка форума';
$this->params['breadcrumbs'][] = ['label' => 'Forumpics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forumpic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
