<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'title',
            [
                'format' => 'raw',
                'attribute' => 'telephone',
                'value' => function($data){
                    return $data->telephone;
                }
            ],

            'email:email',
            'address',
            'facebook',
            'vk',
            'instagram',
            'twitter',
            'google',
        ],
    ]) ?>

</div>
