<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if($_SERVER['REQUEST_URI'] == '/admin'){
            return $this->redirect('admin/menu/index');
        }else{
            return $this->redirect('menu/index');
        }
    }
}
