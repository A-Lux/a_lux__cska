<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.03.2019
 * Time: 9:40
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][4]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][4]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][4]->metaKey;

?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][0]->url]) ?>"><?= Yii::$app->view->params['menu'][0]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][4]->url]) ?>"><?= Yii::$app->view->params['menu'][4]->text ?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" href="<?= URL::toRoute([Yii::$app->view->params['smenu'][8]->url]) ?>"><?= Yii::$app->view->params['smenu'][8]->name ?></a></li>
            </ul>
        </div>

    </div>
    <div class="gallery-title">
        <h2><?= Yii::$app->view->params['sub'][11]->title; ?></h2>
    </div>

    <div class="container">
        <div class="row">

            <?php if ($events['pagination']->page == 0 && count($rightEvent) > 6) : ?>
                <div class="col-sm-3">
                    <div class="news-left">
                        <div class="news-title">
                            <h6>Лента новостей</h6>
                        </div>
                        <div class="news-block">
                            <?php $m = 0; ?>
                            <?php foreach ($rightEvent as $v) : ?>
                                <?php $m++; ?>
                                <?php if ($m > 6) : ?>
                                    <div class="news-item">
                                        <a href="/event/view?id=<?= $v->id; ?>">
                                            <img src="<?= $v->getImage(); ?>" alt="">
                                            <p><?= $v->name ?></p>
                                            <div class="news-date">
                                                <p><?= $v->getDate() ?></p>
                                            </div>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-sm-9">
                <div class="row">

                    <?php foreach ($events['data'] as $v) : ?>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <a href="/event/view?id=<?= $v->id; ?>">
                                    <img src="<?= $v->getImage(); ?>" alt="">
                                    <p><?= $v->name ?></p>
                                    <div class="news-date">
                                        <p><?= $v->getDate() ?></p>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php endforeach; ?>

                </div>
            </div>

        </div>

        <?= $this->render('/partials/pagination', [
            'pages' => $events['pagination'],
        ]); ?>

        <?= $this->render('/partials/home'); ?>
    </div>

</div>
<!-- END MAIN -->