<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.03.2019
 * Time: 15:39
 */

use yii\helpers\Url;
Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][4]->url])?>"><?=Yii::$app->view->params['menu'][4]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['smenu'][8]->url])?>"><?=Yii::$app->view->params['smenu'][8]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=$event->name;?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">


        <div class="conference-wrap">

            <div class="conference-title">
                <h2><?=$event->name?></h2>
            </div>

            <div class="conference-content">

                <div class="conference-left">

                    <div class="conference-top">
                        <a href="<?=$event->getImage()?>"><img src="<?=$event->getImage()?>" alt="conference"></a>
                    </div>

                </div>

                <div class="conference-right">
                    <?=$event->content?>
                </div>

            </div>

        </div>

        <?= $this->render('/partials/go-back');?>

    </div>

    <!-- END MAIN -->


