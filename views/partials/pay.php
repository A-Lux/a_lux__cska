<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 16:01
 */

?>

<div class="pay-books">

    <h3><?=$text;?></h3>

    <div class="pay-books-input">

        <div class="pay-books-input-item">
            <h6><?=Yii::$app->view->params['translation'][20]->name;?></h6>
            <input type="text"> <span><?=Yii::$app->view->params['translation'][22]->name;?></span>
        </div>

        <div class="pay-books-input-item">
            <h6><?=Yii::$app->view->params['translation'][21]->name;?></h6>
            <input class="phone_us" placeholder="+7 (777) 489 36 54 " type="text"> <span><?=Yii::$app->view->params['translation'][22]->name;?></span>
        </div>

        <input id="pay-books-agree" type="checkbox">
        <label for="pay-books-agree"><?=Yii::$app->view->params['agreement']->text;?></label>

        <div class="check-books-form-pay">
            <div class="check-books-form-pay-left">
                <h6><?=Yii::$app->view->params['translation'][23]->name;?></h6>
            </div>

            <div class="check-books-form-pay-right">
                <form action="">
                    <select>
                        <option>Казахстанские карты Visa/MasterCard</option>
                        <option>Казахстанские карты Visa/MasterCard</option>
                        <option>Казахстанские карты Visa/MasterCard</option>
                        <option>Казахстанские карты Visa/MasterCard</option>
                    </select>

                    <button><?=Yii::$app->view->params['translation'][24]->name;?></button>
                </form>
            </div>
        </div>

    </div>

</div>
