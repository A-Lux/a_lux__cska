<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 11:59
 */

use yii\helpers\Url;

$_SESSION['inner_url'] = $_SERVER['HTTP_REFERER']
?>

<div class="back-main">
    <a href="<?=$_SESSION['inner_url'];?>">
        <img src="/public/images/mandat-arrow.png" alt="arrow">
        <?=Yii::$app->view->params['translation'][30]->name;?>
    </a>
</div>
