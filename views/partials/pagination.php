<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 17:54
 */

use yii\widgets\LinkPager;

?>

<div class="cska-pagination">
    <ul>
        <?php
            echo LinkPager::widget([
                'pagination' => $pages,
                'firstPageLabel' => Yii::$app->view->params['translation'][16]->name,
                'prevPageLabel' => Yii::$app->view->params['translation'][17]->name,
                'nextPageLabel' => Yii::$app->view->params['translation'][18]->name,
                'lastPageLabel' => Yii::$app->view->params['translation'][19]->name,
                'activePageCssClass' => 'active',
                'hideOnSinglePage' => true,
            ]);
        ?>
    </ul>
</div>
