<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 11:33
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][1]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][1]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][1]->metaKey;

?>



<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][1]->url])?>"><?=Yii::$app->view->params['menu'][1]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['smenu'][1]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="org-structure-wrap" style="background-image: url('<?=Yii::$app->view->params['sub'][1]->getImage()?>')">

        <div class="container h-100" >

            <div class="org-structure-title">
                <h2><?= Yii::$app->view->params['sub'][1]->title?></h2>
            </div>

        </div>

    </div>


    <div class="org-structure-content">

        <div class="container">

            <div class="table-accordeon-wrap">
                <div class="table-accordeon-content">

                    <div class="table-administration department-accordeon">
                        <a class="table-administration-link" href="#"><?=$admintitle->text?></a>

                        <div class="table-accordeon">
                            <table>
                                <?php $main = true;?>
                                <?php foreach($admin as $v):?>

                                    <?php if($main):?>
                                        <?php $main = false;?>
                                        <tr>

                                            <th><a href="/site/biography"><?=$v->position?></a></th>
                                            <th><a href="/site/biography"><?=$v->text?></a></th>
                                        </tr>
                                    <?php else:?>
                                        <tr>
                                            <th><?=$v->position?></th>
                                            <th><?=$v->text?></th>
                                        </tr>
                                    <?php endif;?>

                                <?endforeach;?>
                            </table>
                        </div>
                    </div>
                    <?php foreach ($sub as $dep):?>
                        <div class="table-department-science department-accordeon">
                        <a class="table-department-link" href="#"><?=$dep->text?></a>
                        <?php $con = $dep->depItems;?>
                        <?php foreach ($con as $v):?>
                            <div class="table-department-content">
                                <p><?=$v->content?></p>
                                <h4><?=$v->position?></h4>
                                <span>тел. <?=$v->phone?></span>

                            </div>
                        <?php endforeach;?>
                        </div>
                    <?php endforeach;?>


                </div>

                <?= $this->render('/partials/home');?>

            </div>

        </div>

    </div>
    <!-- END MAIN -->


