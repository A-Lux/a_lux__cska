<?php

/* @var $this yii\web\View */

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][0]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][0]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][0]->metaKey;

use yii\helpers\Url;


?>
<div class="online-pay wow slideInRight">
    <a class="online-pay-btn" href="#"><?=$sliderName[0]->text?></a>

    <ul>
        <?php foreach ($sliderCon as $v):?>
            <li><a href="<?=URL::toRoute([$v->url])?>"><?=$v->text?></a></li>
        <?php endforeach;?>
    </ul>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="feedback-form modal-content">
            <form action="">
                <a class="feedback-close" data-dismiss="modal">
                    <img src="/public/images/close-icon.png" alt="">
                </a>
                <div class="title">Оставить заявку</div>
                <input type="text" placeholder="Ваше имя*">
                <input class="phone_us" type="text" placeholder="Номер телефона*">
                <input type="text" placeholder="Почта*">
                <textarea type="text" placeholder="Ваше сообщение*"></textarea>
            </form>
        </div>
    </div>
</div>

<!-- Модальное окно -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="main">

    <div class="main-main">
        <div class="container-fluid">
            <div class="row" >
                <div class="col-lg-5 col-md-5 no-padding responsive-bg" >
                    <div class="main-left wow slideInLeft">
                        <div class="logo-text">
                        <p>Под эгидой ЮНЕСКО</p>
                        </div>
                <a href="<?=URL::toRoute(['site/index'])?>"><img src="/public/images/logo.jpg" alt="Логотип"></a>
                        <ul>
                            <?php foreach($bloka['types'] as $v):?>
                                <li><a href="<?=URL::toRoute([$v->url])?>"><?=$v->text?></a></li>
                           <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 slider_banner">
                    <? foreach ($bloka['slider'] as $v):?>
                    <div class="col-lg-12 col-md-12 main-right slider_banner_items wow slideInRight" style="background-image: url('<?=$v->getImage()?>')"></div>
                    <? endforeach;?>
                </div>
            </div>
        </div>

    </div>

    <div class="main-about-center wow slideInUp">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 no-padding main-about-center-left-bg" style="background-image: url('<?=$blokb[0]->getImage()?>')">
                    <div class="main-about-center-left">
                        <h2><?=$blokb[0]->text?></h2>
                        <a href="<?=$blokb[0]->url?>"><?=$blokb[0]->buttonName?></a>
                    </div>
                </div>

                <div class="col-lg-7 no-padding main-about-center-right-wrap">
                    <div class="main-about-center-right">
                        <?=$blokb[0]->content?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main-activity">
            <div class="row">
                <div class="col-lg-5">
                    <div class="main-activity-title wow bounceInLeft">
                        <h2><?=$eventTitle[0]->text?></h2>
                    </div>
                </div>
            </div>

            <div class="offset-lg-7"></div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="main-activity-tabs">
                        <ul>
                            <li><a href="#tab"><?=Yii::$app->view->params['translation'][0]->name?></a></li>
                            <?foreach ($events as $v):?>
                                <li><a href="#tab<?=$v->id?>"><?=$v->text?></a></li>
                            <? endforeach;?>
                        </ul>
                    </div>
                </div>

                <div class="offset-lg-8"></div>
            </div>

            <div class="main-activity-items">
                <div class="row">

                    <div class="col-lg-9 no-padding">
                        <div class="main-activity-tabs-wrap wow bounceInUp">
                            <div class="main-activity-tabs-items" id="tab">
                                <!-- TAB 1 -->
                                <?php foreach ($eventCon as $v):?>
                                    <div class="main-activity-item">
                                        <img src="<?=$v->getImage()?>">
                                        <h4><?=$v->text?></h4>
                                        <a href="#"><?=$v->getDate()?></a>
                                        <h3><?=$v->eventName?></h3>
                                        <p><?=$v->content?> </p>
                                    </div>
                                <? endforeach;?>

                            </div>
                            <!-- END TAB 1 -->

                            <!-- TAB 2 -->
                            <?php foreach ($events as $v):?>
                            <div class="main-activity-tabs-items" id="tab<?=$v->id?>">
                                <?php $event = $v->eventItems;?>
                                <?php foreach ($event as $con):?>
                                    <div class="main-activity-item">
                                        <img src="<?=$con->getImage()?>" alt="conf">
                                        <h4><?=$con->text?></h4>
                                        <a href="#"><?=$con->getDate()?></a>
                                        <h3><?=$con->eventName?></h3>
                                        <p><?=$con->content?> </p>
                                    </div>
                                <? endforeach;?>
                            </div>
                            <!-- END TAB 2 -->
                            <? endforeach;?>

                        </div>
                    </div>



                    <!-- 4 -->
                    <div class="col-lg-3">
                        <div class="main-recommended wow slideInRight" >
                            <h3><?=Yii::$app->view->params['translation'][1]->name?></h3>
                            <?php foreach ($eventCon as $v):?>
                                <?php if($v->recommend):?>
                                    <div class="main-recommended-1" style='background-image: url(<?=$v->getImage()?>)'>
                                        <div class="main-recommended-item">
                                            <span><?=$v->getDate()?></span>
                                            <h5><?=$v->eventName?></h5>
                                            <h4><?=$v->text?></h4>
                                        </div>
                                    </div>
                                <?endif;?>
                            <? endforeach;?>
                        </div>
                    </div>
                    <!-- END 4 -->

                </div>
            </div>
        </div>
    </div>

    <!-- БИЛЕТЫ НА ВЫСТАВКИ -->
    <div class="tickets-wrap wow bounceInUp"  style="background-image: url('<?=$blokc->getImage()?>')">
        <div class="container">
            <div class="row align-items-center" >
                <div class="col-lg-6 tickets-left-content">
                    <div class="tickets-left">
                        <h2><?=$blokc->text?></h2>
                    </div>
                </div>

                <div class="col-lg-6" >
                    <div class="tickets-right">
                        <p><?=$blokc->content?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END БИЛЕТЫ НА ВЫСТАВКИ -->


    <!-- НОВОСТИ -->
    <div class="container">

        <div class="main-news wow slideInUp">

            <div class="row">
                <div class="col-lg-3">
                    <div class="news-title">
                        <h2><?=$news['sub']->text?></h2>
                    </div>
                </div>

                <div class="offset-lg-9"></div>
            </div>

            <div class="row">
                <? if(isset($news['newa'][0])):?>
                <div class="col-lg-3 col-md-3">
                    <div class="news-items-small">
                        <? if($news['newa'][0]->url != null):?>
                            <a href="<?=$news['newa'][0]->url;?>" target="_blank">
                                <img src="<?=$news['newa'][0]->getImage()?>" alt="Новости">
                                <div class="fixed-news-text">
                                <h5><?=$news['newa'][0]->text?></h5>
                                <span><?=$news['newa'][0]->getDate()?></span>
                                </div>
                            </a>
                        <? else:?>
                            <img src="<?=$news['newa'][0]->getImage()?>" alt="Новости">
                            <h5><?=$news['newa'][0]->text?></h5>
                            <span><?=$news['newa'][0]->getDate()?></span>
                        <? endif;?>
                    </div>
                </div>
                <? endif;?>

                <? if(isset($news['newa'][1])):?>
                <div class="col-lg-6 col-md-6 p-0">
                    <div class="news-items-big ">
                        <div class="news-items-big-content">
                            <? if($news['newa'][1]->url != null):?>
                                <a href="<?=$news['newa'][1]->url;?>" target="_blank">
                                    <img src="<?=$news['newa'][1]->getImage()?>" alt="Новости">
                                    <div class="news-items-big-content-span">
                                        <h5><?=$news['newa'][1]->text?></h5>
                                        <span><?=$news['newa'][1]->getDate()?></span>
                                    </div>
                                </a>
                            <? else:?>
                                <img src="<?=$news['newa'][1]->getImage()?>" alt="Новости">
                                <div class="news-items-big-content-span">
                                    <h5><?=$news['newa'][1]->text?></h5>
                                    <span><?=$news['newa'][1]->getDate()?></span>
                                </div>
                            <? endif;?>
                        </div>
                    </div>
                </div>
                <? endif;?>

                <? if(isset($news['newa'][2])):?>
                <div class="col-lg-3 col-md-3">
                    <div class="news-items-small">
                        <? if($news['newa'][2]->url != null):?>
                            <a href="<?=$news['newa'][2]->url;?>" target="_blank">
                                <img src="<?=$news['newa'][2]->getImage()?>" alt="Новости">
                                <div class="fixed-news-text">
                                <h5><?=$news['newa'][2]->text?></h5>
                                <span><?=$news['newa'][2]->getDate()?></span>
                                </div>
                                
                            </a>
                        <? else:?>
                            <img src="<?=$news['newa'][2]->getImage()?>" alt="Новости">
                            <h5><?=$news['newa'][2]->text?></h5>
                            <span><?=$news['newa'][2]->getDate()?></span>
                        <? endif;?>
                    </div>
                </div>
                <? endif;?>

            </div>

            <? if(count($news['news']) > 3):?>
            <div class="news-text">
                <div class="row">

                    <?php $m=0;?>
                    <?php foreach ($news['news'] as $v):?>
                        <?php $m++;?>
                        <?php if ($m > 3):?>
                            <div class="col-lg-3 ">
                                <div class="news-text-items ">
                                    <? if($v->url != null):?>
                                        <a href="<?=$v->url;?>" target="_blank">
                                            <h5><?=$v->text?></h5>
                                            <span><?=$v->getDate()?></span>
                                        </a>
                                    <? else:?>
                                        <h5><?=$v->text?></h5>
                                        <span><?=$v->getDate()?></span>
                                    <? endif;?>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>

                </div>
            </div>
            <? endif?>
        </div>

    </div>
    <!-- END НОВОСТИ -->

    <!-- КОНТАКТЫ -->
    <div class="container-fluid">

        <div class="main-contacts">
            <div class="row">
                <div class="col-lg-7 no-padding">
                    <div class="contacts-left wow slideInLeft">
                        <iframe style="width: 100%;" src="https://yandex.kz/map-widget/v1/-/CCQdNRfhHA?lang=<?=$lang;?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-lg-5 no-padding">
                    <div class="contacts-right wow slideInRight">
                        <div class="contacts-right-title">
                            <h2><?=$contact->title?></h2>
                        </div>

                        <div class="contacts-location contacts-inner-items">
                            <img src="/public/images/main-location.png" alt="Локация">
                            <span><?=$contact->address?></span>
                        </div>

                        <div class="contacts-phone contacts-inner-items">
                            <img src="/public/images/main-phone.png" alt="Телефон">
                            <span><?=$contact->telephone?></span>
                        </div>

                        <div class="contacts-message contacts-inner-items">
                            <img src="/public/images/main-message.png" alt="Сообщение">
                            <span><?=$contact->email?></span>
                        </div>

                        <div class="contacts-btn">

                            <a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][19]->url);?>"><?=Yii::$app->view->params['translation'][2]->name?></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END КОНТАКТЫ -->
</div>
