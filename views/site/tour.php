<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 11:02
 */

use yii\helpers\Url;
Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>


<style type="text/css">
    @-ms-viewport { width: device-width; }
    div#tourDIV {
        height:100%;
        position:relative;
        overflow:hidden;
    }
    div#panoDIV {
        height:100%;
        position:relative;
        overflow:hidden;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
	.tur-item {
    width: 100%;
    height: 30rem;
	display:flex;
}
	div#tourDIV {
    height: 100%;
    position: relative;
    overflow: hidden;
    width: 100%;
    display: flex;
    justify-content: center;
}
	.cursorMoveMode{
		width:80%;
	}
</style>
<script type="text/javascript" src="/indexdata/lib/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="/indexdata/lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/indexdata/lib/jquery-2.1.1.min.js"></script>


<style type="text/css">
    div#panoDIV.cursorMoveMode {
        cursor: move;
        cursor: url(/indexdata/graphics/cursors_move_html5.cur), move;
    }
    div#panoDIV.cursorDragMode {
        cursor: grab;
        cursor: -moz-grab;
        cursor: -webkit-grab;
        cursor: url(/indexdata/graphics/cursors_drag_html5.cur), default;
    }
</style>

<script type="text/javascript">

    function readDeviceOrientation() {
        // window.innerHeight is not supported by IE
        var winH = window.innerHeight ? window.innerHeight : jQuery(window).height();
        var winW = window.innerWidth ? window.innerWidth : jQuery(window).width();
        //force height for iframe usage
        if(!winH || winH == 0){
            winH = '100%';
        }
        // set the height of the document
        jQuery('html').css('height', winH);
        // scroll to top
        window.scrollTo(0,0);
    }
    jQuery( document ).ready(function() {
        if (/(iphone|ipod|ipad|android|iemobile|webos|fennec|blackberry|kindle|series60|playbook|opera\smini|opera\smobi|opera\stablet|symbianos|palmsource|palmos|blazer|windows\sce|windows\sphone|wp7|bolt|doris|dorothy|gobrowser|iris|maemo|minimo|netfront|semc-browser|skyfire|teashark|teleca|uzardweb|avantgo|docomo|kddi|ddipocket|polaris|eudoraweb|opwv|plink|plucker|pie|xiino|benq|playbook|bb|cricket|dell|bb10|nintendo|up.browser|playstation|tear|mib|obigo|midp|mobile|tablet)/.test(navigator.userAgent.toLowerCase())) {
            if(/iphone/.test(navigator.userAgent.toLowerCase()) && window.self === window.top){
                jQuery('body').css('height', '100.18%');
            }
            // add event listener on resize event (for orientation change)
            if (window.addEventListener) {
                window.addEventListener("load", readDeviceOrientation);
                window.addEventListener("resize", readDeviceOrientation);
                window.addEventListener("orientationchange", readDeviceOrientation);
            }
            //initial execution
            setTimeout(function(){readDeviceOrientation();},10);
        }
    });


    function accessWebVr(curScene, curTime){

        unloadPlayer();

        loadPlayer(true, curScene, curTime);
    }
    function accessStdVr(curScene, curTime){

        unloadPlayer();

        loadPlayer(false, curScene, curTime);
    }
    function loadPlayer(isWebVr, curScene, curTime) {
        if (isWebVr) {
            embedpano({
                id:"krpanoSWFObject"
                ,xml:"/indexdata/index_vr.xml"
                ,target:"panoDIV"
                ,passQueryParameters:true
                ,bgcolor:"#000000"
                ,html5:"only+webgl"
                ,focus: false
                ,vars:{skipintro:true,norotation:true,startscene:curScene,starttime:curTime }
            });
        } else {

            var isBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent);
            embedpano({
                id:"krpanoSWFObject"

                ,swf:"/indexdata/index.swf"

                ,target:"panoDIV"
                ,passQueryParameters:true
                ,bgcolor:"#000000"
                ,focus: false
                ,html5:isBot ? "always" : "prefer"
                ,vars:{startscene:curScene,starttime:curTime}

                ,localfallback:"flash"


            });
        }
        //apply focus on the visit if not embedded into an iframe
        if(top.location === self.location){
            kpanotour.Focus.applyFocus();
        }
    }
    function unloadPlayer(){
        if(jQuery('#krpanoSWFObject')){
            removepano('krpanoSWFObject');
        }

    }
    var currentPanotourPlayer = null;
    function getCurrentTourPlayer() {
        if (currentPanotourPlayer == null) {
            currentPanotourPlayer = document.getElementById('krpanoSWFObject');
        }
        return currentPanotourPlayer;
    }
    function isVRModeRequested() {
        var querystr = window.location.search.substring(1);
        var params = querystr.split('&');
        for (var i=0; i<params.length; i++){
            if (params[i].toLowerCase() == "vr"){
                return true;
            }
        }
        return false;
    }
</script>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['smenu'][15]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <div class="tur-wrap">

            <div class="tur-title">
                <h2><?=Yii::$app->view->params['sub'][18]->title;?></h2>
            </div>

            <div class="tur-text">
                <?=$tour->content;?>
            </div>

            <div class="tur-item">
                <div id="tourDIV">
    <div id="panoDIV">
        <noscript>

            <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="100%" height="100%" id="/indexdata/index">
                <param name="movie" value="/indexdata/index.swf"/>
                <param name="allowFullScreen" value="true"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="/indexdata/index.swf" width="100%" height="100%">
                    <param name="movie" value="/indexdata/index.swf"/>
                    <param name="allowFullScreen" value="true"/>
                    <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player to visualize the Virtual Tour : Музей (Виртуальный 3Д-тур(Фото360))"/>
                    </a>
                    <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>

        </noscript>
    </div>



    <script type="text/javascript">
        function sethash(h) { window.top.history.replaceState(undefined, undefined, "#" + h); }
        var myHashchangeHandler = function() { location.reload(); }
        if("addEventListener" in window) { window.addEventListener("hashchange", myHashchangeHandler, false); } else if ("attachEvent" in window) { window.attachEvent( "onhashchange", myHashchangeHandler); }
        var krpano; var startscene; var starthlookat; var startvlookat; var startfov;
        var hash = window.top.location.href.split('#')[1];
        if (typeof hash == 'undefined') { hash = window.location.href.split('#')[1]; }

        function createvars()
        {
            var krpano=document.getElementById('krpanoSWFObject');
            if (typeof hash != 'undefined')
            {
                startscene = hash.split('/')[0];
                starthlookat = hash.split('/')[1];
                startvlookat = hash.split('/')[2];
                startfov = hash.split('/')[3];
            }
            if (startscene && starthlookat && startvlookat && startfov)
            {
                krpano.call('set(startscene,'+startscene+');');
                krpano.call('set(starthlookat,'+starthlookat+');');
                krpano.call('set(startvlookat,'+startvlookat+');');
                krpano.call('set(startfov,'+startfov+');');
            }
        }

        function copyURLToClipboard()
        {
            window.prompt("Copy URL to clipboard", window.top.location);
        }
    </script>











    <script type="text/javascript" src="/indexdata/index.js"></script>
    <script type="text/javascript">
        if (isVRModeRequested()){
            accessWebVr();
        }else{
            accessStdVr();
        }
    </script>
</div>
            </div>

        </div>

        <?= $this->render('/partials/home');?>

    </div>

    <!-- END MAIN -->


