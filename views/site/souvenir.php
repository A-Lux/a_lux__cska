<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 9:19
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][17]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <div class="souvenir-main-wrap">
            <div class="souvenir-main-title">
                <h2><?=Yii::$app->view->params['sub'][19]->title;?></h2>
            </div>

            <div class="souvenir-content">
                <div class="souvenir-main-products">

                    <?php foreach ($souvenir['data'] as $v):?>
                        <div class="souvenir-products-content">
                            <div class="souvenir-products-left">
                                <img src="<?=$v->getImage();?>" alt="Кружка">
                            </div>

                            <div class="souvenir-products-right">
                                <h4><?=$v->name?></h4>

                                <h5><?=Yii::$app->view->params['translation'][5]->name;?></h5>
                                <?=\app\controllers\ContentController::cutStr($v->content, 300)?>

                                <a href="<?=Url::toRoute(['site/souvenirview','id'=>$v->id])?>" class="souvenir-products-btn" style="margin-top:20px;"><?=Yii::$app->view->params['translation'][6]->name;?></a>
                               
                            </div>
                        </div>
                    <?php endforeach;?>



                </div>


            </div>

        </div>

        <?= $this->render('/partials/pagination', [
            'pages'=>$souvenir['pagination'],
        ]);?>


        <?= $this->render('/partials/home');?>

    </div>

    <!-- END MAIN -->



