<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.03.2019
 * Time: 15:38
 */

use yii\helpers\Url;
Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>

<main class="main">
    <div class="container">
        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][2]->url])?>"><?=Yii::$app->view->params['menu'][2]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][5]->name?></a></li>
            </ul>
        </div>
        <div class="middle-title">
            <h3><?=Yii::$app->view->params['sub'][5]->title;?></h3>
        </div>

        <div class="collection-wrapper">

            <?php foreach($forum['data'] as $v):?>

                <a href="<?=URL::toRoute(['site/forumview','id'=>$v->id])?>" class="collection-item">
                    <div class="collection-img">
                        <img src="<?=$v->getImage()?>">
                    </div>
                    <p><?=$v->text?></p>
                    <span><?=$v->getDate();?></span>
                </a>

            <?php endforeach;?>

        </div>


        <?= $this->render('/partials/pagination', [
            'pages'=>$forum['pagination'],
        ]);?>

        <?= $this->render('/partials/home');?>
    </div>
</main>
