<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 10:15
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][18]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <div class="price-list-wrap">
            <div class="price-list-title">
                <?=Yii::$app->view->params['sub'][20]->title;?>
            </div>

            <div class="price-list-content">

                <table class="table-responsive">

                    <tr>
                        <th>№</th>
                        <th><?=$pricesub->contenta?></th>
                        <th><?=$pricesub->unit?></th>
                        <th><?=$pricesub->contentb?></th>
                    </tr>

                    <?php $m = 0;?>
                    <?php foreach ($pricecon as $v):?>
                        <?php $m++;?>
                        <tr>
                            <td><?=$m?></td>
                            <td>
                                <?=$v->contenta?>
                            </td>
                            <td><?=$v->unit?></td>
                            <td><?=$v->contentb?></td>
                        </tr>
                    <?php endforeach;?>
                </table>

            </div>

        </div>

        <?= $this->render('/partials/home');?>

    </div>

