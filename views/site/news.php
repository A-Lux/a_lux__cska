<?php

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][4]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][4]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][4]->metaKey;
?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][0]->url]) ?>"><?= Yii::$app->view->params['menu'][0]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][4]->url]) ?>"><?= Yii::$app->view->params['menu'][4]->text ?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?= Yii::$app->view->params['smenu'][9]->name ?></a></li>
            </ul>
        </div>

    </div>

    <div class="gallery-title">
        <h2><?= Yii::$app->view->params['sub'][12]->title; ?></h2>
    </div>

    <div class="container">
        <div class="row">
            <?php if ($news['pagination']->page == 0 && count($rightNews) > 6) : ?>
                <div class="col-sm-3">
                    <div class="news-left">
                    <div class="news-title">
                        <h6>Лента новостей</h6>
                    </div>
                        <div class="news-block">
                            <?php $m = 0; ?>
                            <?php foreach ($rightNews as $v) : ?>
                                <?php $m++; ?>
                                <?php if ($m > 6) : ?>
                                    <div class="news-block-item">
                                        <? if($v->url != null):?>
                                        <a href="<?= $v->url; ?>" target="_blank">
                                            <p><?= $v->text ?></p>
                                            <div class="news-date">
                                                <p><?= $v->getDate() ?></p>
                                            </div>
                                        </a>
                                        <? else:?>
                                        <p><?= $v->text ?></p>
                                        <div class="news-date">
                                            <p><?= $v->getDate() ?></p>
                                        </div>
                                        <? endif;?>
                                    </div>

                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                </div>
            <?php endif; ?>
            <div class="col-sm-9">
                <div class="row">
                    <?php foreach ($news['data'] as $v) : ?>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news-item">
                                <? if($v->url != null):?>
                                <a href="<?= $v->url; ?>" target="_blank">
                                    <img src="<?= $v->getImage(); ?>" alt="">
                                    <p><?= $v->text ?></p>
                                    <div class="news-date">
                                        <p><?= $v->getDate() ?></p>
                                    </div>
                                </a>
                                <? else:?>
                                <img src="<?= $v->getImage(); ?>" alt="">
                                <p><?= $v->text ?></p>
                                <div class="news-date">
                                    <p><?= $v->getDate() ?></p>
                                </div>
                                <? endif;?>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?= $this->render('/partials/pagination', [
            'pages' => $news['pagination'],
        ]); ?>
        <?= $this->render('/partials/home'); ?>
    </div>

</div>
<!-- END MAIN -->