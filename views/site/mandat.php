<?php

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][1]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][1]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][1]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][1]->url])?>"><?=Yii::$app->view->params['menu'][1]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][0]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="mandat-target-wrap" >

        <div class="container h-100" >
            <div class="mandat-target wow fadeInUp" style="background-image: url('<?=Yii::$app->view->params['sub'][0]->getImage()?>')">
                <h2><?=Yii::$app->view->params['sub'][0]->title;?></h2>
            </div>
        </div>

    </div>


    <div class="container">

        <div class="mandat-content">
            <?=$mandat->content?>

            <?= $this->render('/partials/home');?>
        </div>

    </div>

</div>
<!-- END MAIN -->
