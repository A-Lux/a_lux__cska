<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.03.2019
 * Time: 13:55
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][2]->url])?>"><?=Yii::$app->view->params['menu'][2]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['smenu'][4]->url])?>"><?=Yii::$app->view->params['smenu'][4]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=$content->text?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">


        <div class="conference-wrap">

            <div class="conference-title">
                <h2><?=$content->text?></h2>
            </div>

            <div class="conference-content-inner">
                <div class="conference-slider-inner" id="cascade-slider">
                    <div class="conference-inner">
                        <?foreach ($pictures as $v):?>
                            <div class="conference-bottom-slide">
                            <a href="<?=$v->getImage();?>"><img src="<?=$v->getImage();?>" alt="img"></a>

                            </div>
                        <?php endforeach; ?>
                    </div>


                </div>


                <div class="conference-text">
                    <p><?=$content->content?></p>
                </div>


            </div>

        </div>

        <?= $this->render('/partials/go-back');?>

    </div>

    <!-- END MAIN -->


