<?php

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>


<main class="main">
  <div class="container">
    <div class="bread-crumbs wow fadeInLeft">
        <ul>
            <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
            <span>/</span>
            <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
            <span>/</span>
            <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['smenu'][14]->name?></a></li>
        </ul>
    </div>
    
    <div class="middle-title">
      <h3><?=Yii::$app->view->params['sub'][17]->title;?></h3>
    </div>

    <div class="collection-wrapper">

        <?php foreach($collection['data'] as $v):?>
            <a href="<?=URL::toRoute(['site/collectionview','id'=>$v->id])?>" class="collection-item">
                <div class="collection-img">
                    <img src="<?=$v->getImage()?>">
                </div>
                <p><?=$v->name?></p>
                <span><?=$v->getDate();?></span>
            </a>
        <?php endforeach;?>

    </div>



      <?= $this->render('/partials/pagination', [
          'pages' => $collection['pagination'],
      ]);?>


    <?= $this->render('/partials/home');?>
  </div>
</main>


