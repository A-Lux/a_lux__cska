<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 13:55
 */

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][1]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][1]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][1]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][1]->url])?>"><?=Yii::$app->view->params['menu'][1]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][2]->name?> </a></li>
            </ul>
        </div>

    </div>

    <div class="reports-wrap" style="background-image: url('<?=Yii::$app->view->params['sub'][2]->getImage()?>')">
        <div class="container h-100">

            <div class="reports-title">
                <h2><?= Yii::$app->view->params['sub'][2]->title?></h2>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="reports-content">

            <div class="report-document-wrap">

                <?php foreach($report as $v):?>
                    <div class="report-document-item">
                        <img src="/public/images/document.png" alt="document">
                        <a href="<?=$v->getFile()?>" target="_blank" ><?=$v->text?></a>
                    </div>
                <?php endforeach;?>
                <?= $this->render('/partials/home');?>
            </div>

        </div>
    </div>

</div>
<!-- END MAIN -->
