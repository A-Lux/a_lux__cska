<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.03.2019
 * Time: 13:42
 */

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][5]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][5]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][5]->metaKey;
?>

<main class="main">
    <div class="container">
        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][5]->url])?>"><?=Yii::$app->view->params['menu'][5]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][12]->name?></a></li>
            </ul>
        </div>

        <div class="press-releases">

            <div class="container">

                <div class="row">

                    <div class="col-12">
                        <div class="press-title">
                            <h1><?=Yii::$app->view->params['sub'][15]->title;?></h1>
                        </div>
                    </div>
                </div>

               <?php foreach ($press['data'] as $v):?>
                    <div class="press-content">
                    <div class="row">

                        <div class="col-lg-3">
                            <div class="press-left">
                                <img src="<?=$v->getImage();?>" alt="men">
                            </div>
                        </div>

                        <div class="col-lg-9">
                            <div class="press-right">
                                <h3><?=$v->name;?></h3>

                                <h6><?=Yii::$app->view->params['translation'][8]->name;?></h6>
                                <p><?=$v->subcontent;?></p>

                                <a href="<?=URL::toRoute(['site/pressview','id'=>$v->id])?>" class="press-btn"><?=Yii::$app->view->params['translation'][6]->name;?></a>
                                
                            </div>
                        </div>

                    </div>

                </div>
               <?php endforeach;?>
            </div>
        </div>
    </div>

    <?= $this->render('/partials/pagination', [
        'pages'=>$press['pagination'],
    ]);?>


    <?= $this->render('/partials/home');?>
    </div>
</main>
