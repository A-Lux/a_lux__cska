<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 9:20
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['smenu'][17]->url])?>"><?=Yii::$app->view->params['smenu'][17]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=$content->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <div class="souvenir-inner-wrap">

            <div class="souvenir-inner-title">
                <h2><?=$content->name?></h2>
            </div>

            <div class="souvenir-inner-content">

                <div class="souvenir-inner-left">
                    <img src="<?=$content->getImage();?>" alt="Кружка">
                </div>

                <div class="souvenir-inner-right">
                    <h4><?=$content->name?></h4>
                    <h5><?=Yii::$app->view->params['translation'][5]->name;?></h5>
                    <?=$content->content?>
                    <h3><?=Yii::$app->view->params['translation'][9]->name;?>: <span><?=$content->price?> kzt</span> </h3>
                </div>

            </div>

            <?= $this->render('/partials/pay',[
                'text' => Yii::$app->view->params['translation'][25]->name,
            ]);?>
        </div>

        <?= $this->render('/partials/go-back');?>


    </div>

    <!-- END MAIN -->


