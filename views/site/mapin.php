<?php

Yii::$app->view->params['title'] = $city->name;

use yii\helpers\Url; ?>

<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a href="#"><?=Yii::$app->view->params['smenu'][16]->name?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['submenu'][2]->url])?>"><?=Yii::$app->view->params['submenu'][2]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=$city->name;?></a></li>
            </ul>
        </div>

    </div>
    <div class="gallery-title">
        <h2><?=$city->name;?></h2>
    </div>

    <div class="container">


        <?=$city->description;?>

        <br>
        <? if(count($sliders) > 0):?>



            <div class="gallery-content-wrap" id="gallery-content-wrap">
                <div class="gallery-content" id="gallery-content">
                    <div class="gallery-item">

                        <?php foreach ($sliders as $v):?>
                            <a href="<?=$v->getImage();?>"><span class="gallery-zoom"><img src="/public/images/zoom.png" alt="zoom"></span><img src="<?=$v->getImage();?>" alt="gallery image"></a>
                        <?php endforeach;?>
                    </div>
                </div>

            </div>

        <? endif;?>

        <?= $this->render('/partials/go-back');?>

    </div>
</div>
