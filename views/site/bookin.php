<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 13:20
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>

<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][2]->url])?>"><?=Yii::$app->view->params['menu'][2]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['submenu'][1]->url])?>"><?=Yii::$app->view->params['submenu'][1]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=$content->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <div class="books-inner-wrap">

            <div class="books-inner-title">
                <h2>Книга</h2>
            </div>

            <div class="books-inner-content">

                <div class="books-inner-left">
                    <img src="<?=$content->getImage()?>" alt="История Казахстана">
                </div>

                <div class="books-inner-right">
                    <h5><?=Yii::$app->view->params['translation'][3]->name;?></h5>
                    <h6><?=$content->author?></h6>

                    <h5><?=Yii::$app->view->params['translation'][4]->name;?></h5>
                    <h6><?=$content->name?></h6>

                    <h5 style="color: #000"><?=Yii::$app->view->params['translation'][5]->name;?></h5>
                    <?=$content->content?>

                    <h3><?=Yii::$app->view->params['translation'][7]->name;?>: <?=$content->contentName?>
                   
                </div>

            </div>

            <?= $this->render('/partials/pay',[
                    'text' => Yii::$app->view->params['translation'][26]->name,
            ]);?>

        </div>

        <?= $this->render('/partials/go-back');?>

    </div>

    <!-- END MAIN -->



