<?php

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][5]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][5]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][5]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][0]->url]) ?>"><?= Yii::$app->view->params['menu'][0]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][5]->url]) ?>"><?= Yii::$app->view->params['menu'][5]->text ?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?= Yii::$app->view->params['smenu'][10]->name ?></a></li>
            </ul>
        </div>

    </div>

    <div class="gallery-wrap">

        <div class="container">

            <div class="gallery-title">
                <h2><?= Yii::$app->view->params['sub'][13]->title; ?></h2>
            </div>

            <div class="gallery-content-wrap" id="gallery-content-wrap">
                <div class="gallery-content" id="gallery-content">

                    <div class="gallery-item">
                        <?php foreach ($galleries as $v) : ?>
                            <a href="<?= $v->getImage(); ?>"><span class="gallery-zoom"><img src="/public/images/zoom.png" alt="zoom"></span><img src="<?= $v->getImage(); ?>" alt="gallery image"></a>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

            <?= $this->render('/partials/home'); ?>

        </div>

    </div>

</div>

<!-- END MAIN -->
