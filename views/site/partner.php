<?php

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][3]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][3]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][3]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['menu'][3]->text?> </a></li>
            </ul>
        </div>

    </div>

    <div class="gallery-title">
        <h2><?= Yii::$app->view->params['sub'][10]->title?></h2>
    </div>

    <div class="container">
        <div class="row">

            <?php foreach ($partners['data'] as $v):?>

                <div class="col-lg-2 col-md-4 col-sm-6">
                    <div class="partner-item">
                        <a href="<?=$v->url?>"><img src="<?=$v->getImage();?>" alt=""></a>
                    </div>
                </div>

            <?php endforeach;?>

            <?= $this->render('/partials/pagination', [
                'pages'=>$partners['pagination'],
            ]);?>


        </div>
        <?= $this->render('/partials/home');?>
    </div>

</div>
<!-- END MAIN -->
