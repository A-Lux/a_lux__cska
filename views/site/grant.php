<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 14:23
 */

use yii\helpers\Url;
use yii\widgets\LinkPager;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>

<main class="main">
    <div class="container">
        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][2]->url])?>"><?=Yii::$app->view->params['menu'][2]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][3]->name?></a></li>
            </ul>
        </div>
        <div class="middle-title">
            <h3><?=Yii::$app->view->params['sub'][3]->title;?></h3>
        </div>
        <div class="grant-projects">
            <?php foreach($projects['project'] as $v):?>
            <div class="project-item">
                <img src="<?=$v->getImage()?>">
                <div class="project-content">
                    <a href="<?=URL::toRoute(['site/projectview','id'=>$v->id])?>"><?=$v->name?></a>
                    <p><?=$v->subcontent?></p>
                </div>
            </div>
            <?php endforeach;?>
        </div>

        <?= $this->render('/partials/pagination', [
            'pages'=>$projects['pagination'],
        ]);?>

        <?= $this->render('/partials/home');?>

    </div>
</main>
