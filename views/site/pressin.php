<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.03.2019
 * Time: 13:49
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][5]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][5]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][5]->metaKey;

?>

<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][0]->url]) ?>"><?= Yii::$app->view->params['menu'][0]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][5]->url]) ?>"><?= Yii::$app->view->params['menu'][5]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['smenu'][12]->url]) ?>"><?= Yii::$app->view->params['smenu'][12]->name ?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?= $content->name ?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">
        <div class="cascade-slider_container" id="cascade-slider">
            <div class="cascade-slider_slides">
                <div class="cascade-slider_item">
                    <h3>Cat 1</h3>
                    <img src="http://cat-wallpaper.ambientcat.com/wallpapers-720-720/Maine-Coon-Silver-kitten-Fabulous-breeds-of-cats-with-big-paws-177.jpg" alt="">
                </div>
                <div class="cascade-slider_item">
                    <h3>Cat 2</h3>
                    <img src="http://cat-background.ambientcat.com/Backgrounds-720-720/Understanding-Feline-Language-Tricolor-kitten-81.jpg" alt="">
                </div>
                <div class="cascade-slider_item">
                    <h3>Cat 3</h3>
                    <img src="https://s-media-cache-ak0.pinimg.com/736x/43/e9/4f/43e94f99ba26cef2bd8d4596049a7e9f.jpg" alt="">
                </div>
                <div class="cascade-slider_item">
                    <h3>Cat 4</h3>
                    <img src="http://cat-wallpaper.ambientcat.com/wallpapers-720-720/Maine-Coon-Silver-kitten-Fabulous-breeds-of-cats-with-big-paws-177.jpg" alt="">
                </div>
                <div class="cascade-slider_item">
                    <h3>Cat 5</h3>
                    <img src="http://r.ddmcdn.com/s_f/o_1/cx_653/cy_429/cw_1296/ch_1296/w_720/APL/uploads/2012/05/cat-toy-article.jpg" alt="">
                </div>

            </div>

            <ol class="cascade-slider_nav">
                <li class="cascade-slider_dot cur"></li>
                <li class="cascade-slider_dot"></li>
                <li class="cascade-slider_dot"></li>
                <li class="cascade-slider_dot"></li>
                <li class="cascade-slider_dot"></li>
                <li class="cascade-slider_dot"></li>
            </ol>

            <span class="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev">Prev</span>
            <span class="cascade-slider_arrow cascade-slider_arrow-right" data-action="next">Next</span>
        </div>

        <div class="conference-wrap">

            <div class="conference-title">
                <h2><?= $content->name ?></h2>
            </div>

            <div class="conference-content-inner">

                <div class="conference-slider-inner" id="cascade-slider">
                    <?php foreach ($pictures as $v) : ?>
                        <a href="<?= $v->getImage(); ?>"><img src="<?= $v->getImage(); ?>" alt="img"></a>
                    <?php endforeach; ?>
                </div>

                <div class="conference-text">
                    <?= $content->content ?>
                </div>
            </div>
        </div>

        <?= $this->render('/partials/go-back'); ?>

    </div>

    <!-- END MAIN -->