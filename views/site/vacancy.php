<?php

use yii\helpers\Url;

$email = explode( ',', Yii::$app->view->params['contact']->email);

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][7]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][7]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][7]->metaKey;
?>

<main class="main">
    <div class="container">
      <div class="bread-crumbs wow fadeInLeft">
          <ul>
              <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
              <span>/</span>
              <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][7]->url])?>"><?=Yii::$app->view->params['menu'][7]->text?></a></li>
              <span>/</span>
              <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['smenu'][20]->name?></a></li>
          </ul>
      </div>
      <div class="middle-title">
        <h3><?=Yii::$app->view->params['sub'][8]->title;?></h3>
      </div>

      <div class="vacancy-wrapper">

      <?php if(count($vacancies['data']) == 0):?>
          <div class="no-vacancy wow slideInRight">
              <div class="bank-text">
				  <p><?=Yii::$app->view->params['translation'][13]->name;?></p>
                  <a href="mailto:<?=$email[0]?>"><?=$email[0]?></a>
              </div>
          </div>
      <?php endif;?>

      <?php foreach ($vacancies['data'] as $v):?>

        <div class="vacancy-item">
          <h3><?=$v->name?></h3>
          <p><?=$v->content?></p>
          <div class="price">
              <p><span><?=$v->price?> тг</span> / <?=Yii::$app->view->params['translation'][14]->name;?></p>
          </div>
          <ul>
            <li><a href="#"><?=$v->address?></a></li>
            <li><a><?=$v->getDate()?></a></li>
            <li><a><?=$v->employment?></a></li>
          </ul>
        </div>

      <?php endforeach;?>


      </div>

        <?= $this->render('/partials/pagination', [
            'pages'=>$vacancies['pagination'],
        ]);?>

        <?= $this->render('/partials/home');?>
    </div>
</main>


