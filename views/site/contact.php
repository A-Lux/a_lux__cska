<?php


use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][7]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][7]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][7]->metaKey;


$tel = explode( ',', Yii::$app->view->params['contact']->telephone);
$email = explode( ',', Yii::$app->view->params['contact']->email);
?>
<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">

            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][7]->url])?>"><?=Yii::$app->view->params['menu'][7]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['smenu'][19]->name?></a></li>
            </ul>

        </div>

    </div>
    <div class="gallery-title">
        <h2><?=Yii::$app->view->params['sub'][7]->title;?></h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="feedback">
                    <h3><?=$feedsub->text?></h3>
                </div>

                <div class="contacts-location contacts-inner-items">
                    <img src="/public/images/main-location.png" alt="Локация">
                    <span><?=Yii::$app->view->params['contact']->address?></span>
                </div>

                <div class="contacts-phone contacts-inner-items">
                    <img src="/public/images/main-phone.png" alt="Телефон">
                    <span>
                        <a href="tel:<?=$tel[0];?>"><?=$tel[0];?></a>
                        <a href="tel:<?=$tel[1];?>"><?=$tel[1];?></a>
                    </span>
                </div>

                <div class="contacts-message contacts-inner-items">
                    <img src="/public/images/main-message.png" alt="Сообщение">
                    <span><a href="<?=Yii::$app->view->params['contact']->email;?>">
                        <?=Yii::$app->view->params['contact']->email;?></a></span>
                </div>
                <div class="socials">
                    <a href="<?=Yii::$app->view->params['contact']->facebook?>" target="_blank"><img src="/public/images/soc1.png" alt=""></a>
                    <a href="<?=Yii::$app->view->params['contact']->vk?>" target="_blank"><img src="/public/images/soc2.png" alt=""></a>
                    <a href="<?=Yii::$app->view->params['contact']->instagram?>" target="_blank"><img src="/public/images/soc3.png" alt=""></a>
                    <a href="<?=Yii::$app->view->params['contact']->twitter?>" target="_blank"><img src="/public/images/soc4.png" alt=""></a>
                    <a href="<?=Yii::$app->view->params['contact']->google?>" target="_blank"><img src="/public/images/soc5.png" alt=""></a>
                </div>

            </div>
            <div class="col-sm-6">
                <?php if(Yii::$app->session->getFlash('request')):?>
                    <div class="alert alert-success" role="alert">
                        <?= Yii::$app->session->getFlash('request'); ?>
                    </div>
                <?php endif;?>
                <form action="/site/request" method="get" >
                    <div class="contact">

                        
                            <textarea name = "content" type="textarea" placeholder="<?=Yii::$app->view->params['translation'][10]->name;?>" required></textarea> <br>
                       
                        <input name="name" type="text" placeholder="<?=Yii::$app->view->params['translation'][11]->name;?>" required> <br>
                        
                        <input name="email" type="email" placeholder="<?=Yii::$app->view->params['translation'][12]->name;?>" required>
                    </div>
                    <div class="contacts-btn no-top">
                        <button ><?=$feedsub->buttonName?></button>
                    </div>

                </form>
            </div>
        </div>

    </div>

</div>

<? if(Yii::$app->session['lang'] == '_en'):?>
    <? $lang = 'en_US';?>
<? elseif(Yii::$app->session['lang'] == '_kz'):?>
    <? $lang = 'kz_KZ';?>
<? else:?>
    <? $lang = 'ru_RU';?>
<? endif;?>
<!-- END MAIN -->
<div class="map">
<!--    <iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2177.179037963574!2d76.90879958724774!3d43.24753625819258!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836937d48929c3%3A0x53e4dd55c40c8423!2z0YPQuy4g0JrQsNCx0LDQvdCx0LDQuSDQsdCw0YLRi9GA0LAgOTQsINCQ0LvQvNCw0YLRiyAwNTAwMDA!5e0!3m2!1sru!2skz!4v1591349897471!5m2!1sru!2skz" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>-->

        <iframe style="width: 100%" src="https://yandex.kz/map-widget/v1/-/CCQdNRfhHA?lang=<?=$lang;?>" width="100%" height="490"  frameborder="1" allowfullscreen="true"></iframe>
</div>
