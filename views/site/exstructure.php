<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.03.2019
 * Time: 16:47
 */

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>


<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][13]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="ex-structure-wrap" style="background-image: url('<?=Yii::$app->view->params['sub'][16]->getImage();?>')">

        <div class="container h-100">

            <div class="ex-structure-title">
                <h2><?=Yii::$app->view->params['sub'][16]->title;?></h2>
            </div>

        </div>

    </div>


    <div class="ex-structure-content">

        <div class="container">

            <div class="table-accordeon-wrap">
                <div class="table-accordeon-content">

                    <?php foreach ($exstrucadmin as $v):?>

                        <div class="table-administration department-accordeon">
                            <a class="table-administration-link" href="#"><?=$v->text?></a>

                            <div class="table-accordeon">
                                <?=$v->content?>
                            </div>
                        </div>

                    <?php endforeach;?>

                    <?php foreach ($exstrucdep as $dep):?>

                    <div class="table-department-science department-accordeon">
                        <a class="table-department-link" href="#"><?=$dep->name?></a>
                        <?php $con = $dep->depItems;?>
                        <?php foreach ($con as $v):?>
                            <div class="table-department-content">
                                <p><?=$v->content?></p>
                                <h4><?=$v->position?></h4>
                                <span>тел. <?=$v->phone?></span>

                            </div>
                        <?php endforeach;?>
                    </div>

                    <?php endforeach;?>

                </div>

                <?= $this->render('/partials/home');?>

            </div>

        </div>

    </div>
</div>
    <!-- END MAIN -->


