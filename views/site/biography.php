<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 13:11
 */

use yii\helpers\Url;


?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">
        <div class="mandat-content">
            <?=$model->content?>
        </div>
        <?= $this->render('/partials/home');?>
    </div>

</div>
<!-- END MAIN -->

