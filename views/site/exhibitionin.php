<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.03.2019
 * Time: 16:04
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;

?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][0]->url]) ?>"><?= Yii::$app->view->params['menu'][0]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['menu'][2]->url]) ?>"><?= Yii::$app->view->params['menu'][2]->text ?></a></li>
                <span>/</span>
                <li><a href="<?= URL::toRoute([Yii::$app->view->params['smenu'][6]->url]) ?>"><?= Yii::$app->view->params['smenu'][6]->name ?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?= $content->text ?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">


        <div class="conference-wrap">

            <div class="conference-title">
                <h2><?= $content->text ?></h2>
            </div>

            <div class="conference-content">

                <div class="conference-left">

                    <div class="conference-top">
                        <a href="<?= $content->getImage() ?>"><img src="<?= $content->getImage() ?>" alt="conference"></a>
                    </div>

                    <div class="conference-bottom" id="conference-bottom">
                        <? $m=0;?>

                        <?php for ($i = 0; $i < count($pictures) / 4; $i++) : ?>

                            <?php for ($k = 0; $k < count($pictures); $k++) : ?>
                                <?php if ($m < ($i + 1) * 4 && $m < count($pictures)) : ?>
                                    <div class="conference-bottom-slide">
                                        <a href="<?= $pictures[$m]->getImage() ?>"><img src="<?= $pictures[$m]->getImage() ?>" alt="conference"></a>
                                        <?php $m++; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endfor; ?>

                        <?php endfor; ?>

                    </div>

                </div>


                <div class="conference-right">
                    <p><?= $content->content ?></p>
                </div>

            </div>

        </div>

        <?= $this->render('/partials/go-back'); ?>

    </div>

    <!-- END MAIN -->