<?php

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][6]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][6]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][6]->metaKey;
?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][6]->url])?>"><?=Yii::$app->view->params['menu'][6]->text?></a></li>
                <span>/</span>
                <li><a href="#"><?=Yii::$app->view->params['smenu'][16]->name?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['submenu'][3]->name?></a></li>
            </ul>
        </div>

    </div>

    <div class="container">

        <img src="<?=$model->getImage();?>" width="100%" height="100%">

        <?= $this->render('/partials/home');?>
    </div>
</div>
