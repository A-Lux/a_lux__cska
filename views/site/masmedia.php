<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.03.2019
 * Time: 13:41
 */

use yii\helpers\Url;


Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][5]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][5]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][5]->metaKey;
?>

<main class="main">
    <div class="container">
        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][5]->url])?>"><?=Yii::$app->view->params['menu'][5]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][11]->name?></a></li>
            </ul>
        </div>
        <div class="middle-title">
            <h3><?=Yii::$app->view->params['sub'][14]->title;?></h3>
        </div>

        <div class="books-main">
            <ul class="tabs">
                <?php $active = "current";?>
                    <?php foreach ($mastab as $v):?>
                    <li class="tab-link <?=$active?>" data-tab="tab-<?=$v['id']?>"> <?=$v['title']?></li>
                    <?php $active = "";?>
                <?php endforeach;?>

            </ul>

            <?php $active = "current";?>
            <?php foreach ($mastab as $v):?>
                <div id="tab-<?=$v['id']?>" class="tab-content <?=$active?>">
                    <?php $content = $v['content'];?>
                    <?php foreach ($content['data'] as $con):?>
                        <div class="article-item">
                            <?=$con->content?>
                        </div>
                    <?php endforeach;?>
                    <?= $this->render('/partials/pagination', [
                        'pages'=>$content['pagination'],
                    ]);?>
                </div>

                <?php $active = "";?>
            <?php endforeach;?>

        </div>

        <?= $this->render('/partials/home');?>
    </div>
</main>
