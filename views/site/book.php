<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 05.03.2019
 * Time: 13:19
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>

<main class="main">
    <div class="container">
        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][2]->url])?>"><?=Yii::$app->view->params['menu'][2]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active"><?=Yii::$app->view->params['submenu'][1]->name?></a></li>
            </ul>
        </div>
        <div class="middle-title">
            <h3><?=Yii::$app->view->params['sub'][23]->title;?></h3>
        </div>

        <div class="books-main">

            <ul class="tabs">
                <?php $active = "current";?>
                <?php foreach ($books as $v):?>
                    <li class="tab-link <?=$active?>" data-tab="tab-<?=$v['id']?>"> <?=$v['title']?></li>
                    <?php $active = "";?>
                <?php endforeach;?>
            </ul>

            <?php $active = "current";?>
            <?php foreach ($books as $v):?>
                <div id="tab-<?=$v['id']?>" class="tab-content <?=$active?>">
                    <?php $content = $v['content'];?>
                    <?php foreach ($content['data'] as $con):?>
                        <div class="books-item">
                            <div class="books-img">
                                <img src="<?=$con->getImage();?>">
                            </div>

                            <div class="books-content">

                               	<p><?=Yii::$app->view->params['translation'][3]->name;?>: <span><?=$con->author?></span></p>
                                <p><?=Yii::$app->view->params['translation'][4]->name;?>: <span><?=$con->name?></span></p>

                                <div class="about-book">
                                    <h5><?=Yii::$app->view->params['translation'][5]->name;?></h5>
                                    <?=\app\controllers\ContentController::cutStr($con->content, 300)?>
                                </div>
                                <a href="<?=Url::toRoute(['site/bookview','id'=>$con->id])?>" class="more-btn">
                                   <?=Yii::$app->view->params['translation'][6]->name;?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach;?>
                    <?= $this->render('/partials/pagination', [
                        'pages'=>$content['pagination'],
                    ]);?>

                </div>
                <?php $active = "";?>
            <?php endforeach;?>
        </div>

        <?= $this->render('/partials/home');?>
</main>
