<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 13:11
 */

use yii\helpers\Url;

Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][7]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][7]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][7]->metaKey;
?>

<!-- MAIN -->
<div class="main">

    <div class="container">

        <div class="bread-crumbs wow fadeInLeft">
            <ul>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][0]->url])?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                <span>/</span>
                <li><a href="<?=URL::toRoute([Yii::$app->view->params['menu'][7]->url])?>"><?=Yii::$app->view->params['menu'][7]->text?></a></li>
                <span>/</span>
                <li><a class="bread-crumbs-active" ><?=Yii::$app->view->params['smenu'][21]->name?></a></li>
            </ul>
        </div>

    </div>
    <div class="gallery-title">
        <h2><?=Yii::$app->view->params['sub'][9]->title;?></h2>
    </div>

    <div class="container">
        <div class="mandat-content">
            <img src="<?=$blog->getImage()?>" class="img-fluid " alt="">
            <?=$blog->content?>
        </div>

        <?= $this->render('/partials/home');?>

    </div>

</div>
<!-- END MAIN -->

