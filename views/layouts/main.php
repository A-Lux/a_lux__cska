
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);
$subarr =  array();
foreach (Yii::$app->view->params['submenu'] as $v):
    array_push($subarr,$v->submenu_id);
endforeach;


$tel = explode( ',', Yii::$app->view->params['contact']->telephone);
$email = explode( ',', Yii::$app->view->params['contact']->email);

if(Yii::$app->session['lang'] == '_kz'){
    $lang = 'kz';
}elseif(Yii::$app->session['lang'] == '_en'){
    $lang = 'en';
}else{
    $lang = 'ru';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= $lang ?>">
<head>
    <title><?=Yii::$app->view->params['title']?></title>
    <meta name="Description" content="<?=Yii::$app->view->params['desc']?>">
    <meta name="Keywords" content="<?=Yii::$app->view->params['key']?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="cube-loader">
    <div class="caption">
        <div class="cube-loader">
            <div class="cube loader-1"></div>
            <div class="cube loader-2"></div>
            <div class="cube loader-4"></div>
            <div class="cube loader-3"></div>
        </div>
    </div>
</div>
<!-- end preloader -->

<!-- HEADER -->
<header class="header" id="header">

    <div class="container no-padding">
        <!-- TOP -->
        <div class="header-top wow fadeInLeft">
            <div class="header-logo">
                <?=Yii::$app->view->params['mainslidea']->text;?>
            </div>

            <div class="header-items-wrap">
                <div class="header-phone header-items">
                    <?php foreach ($tel as $v):?>
                        <h6><a href="tel:<?=$v;?>"><?=$v;?></a></h6>
                    <?php endforeach;?>
                </div>

                <div class="header-items-mes-loc">
                    <div class="header-message header-items">
                        <h6><a href="mailto:<?=$email[0];?>"><?=$email[0];?></a></h6>
                    </div>

                    <div class="header-location header-items">
                        <h6><?=Yii::$app->view->params['contact']->address?></h6>
                    </div>
                </div>

                <div class="header-lang" id="header-lang">

                        <a class="dropdown-item" href="<?=Url::toRoute(['/lang/index','url'=>'kz'])?>">
                            <img src="/public/images/main-lang-kz.png" alt="RU">
                            <span></span>
                        </a>
                        <a class="col-sm dropdown-item" href="<?=Url::toRoute(["/lang/index", 'url' => 'ru'])?>">
                            <img src="/public/images/main-lang.png" alt="KZ">
                            <span></span>
                        </a>
                        <a class="col-sm dropdown-item" href="<?=Url::toRoute(["/lang/index", 'url' => 'en'])?>">
                            <img src="/public/images/main-lang-en.png" alt="EN">
                            <span></span>
                        </a>

                </div>
            </div>
        </div>
        <!-- END TOP -->
    </div>

    <!-- MENU -->
    <div class="header-menu wow slideInRight">

        <div class="container no-padding">
            <div class="hamburger-menu-btn">
                <a href="#">
                    <img src="/public/images/menu-button.svg" alt="menu button">
                </a>
            </div>


            <!-- MOBILE MENU -->
            <nav class="mobile-menu">
                <ul>
                    <?php foreach (Yii::$app->view->params['menu'] as $v):?>
                        <?php if($v->status):?>
                            <?php $class = "";?>
                            <?php if($v->menuItems != null):?>
                                <?php $class="submenu-arrow submenu-head";?>
                            <?php endif;?>
                            <li>
                                <a class="<?=$class?>" href="<?=URL::toRoute([$v->url])?>"><?=$v->text?></a>
                                <?php $submenu = $v->menuItems;?>
                                <?php if($submenu != null):?>
                                <ul class="mobile-submenu">
                                <?php endif;?>
                                <?php foreach ($submenu as $subv):?>
                                    <?php if($subv->status):?>
                                        <?php $class = "";?>
                                        <?php if(in_array($subv->id,$subarr)):?>
                                            <?php $class="submenu-arrow mobile-submenu-link";?>
                                        <?php endif;?>
                                        <li>
                                            <a class="<?=$class?>" href="<?=URL::toRoute([$subv->url])?>"><?=$subv->name?></a>
                                            <?php if(in_array($subv->id,$subarr)):?>
                                                <ul class="mobile-submenu-sub">
                                                <?php $class="submenu-arrow mobile-submenu-link";?>
                                            <?php endif;?>
                                                <?php foreach (Yii::$app->view->params['submenu'] as $subsubv):?>
                                                    <?php if($subsubv->status):?>
                                                        <?php if($subsubv->submenu_id == $subv->id):?>
                                                            <li><a href="<?=URL::toRoute([$subsubv->url])?>"><?=$subsubv->name?></a></li>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php if(in_array($subv->id,$subarr)):?>
                                                </ul>
                                            <?php endif;?>
                                        </li>
                                    <?php endif;?>
                                <?php endforeach;?>
                                <?php if($submenu != null):?>
                                </ul>
                            <?php endif;?>
                            </li>
                        <?php endif;?>
                    <?php endforeach;?>

                </ul>
            </nav>
            <!-- END MOBILE MENU -->

            <!-- DESKTOP MENU -->
            <nav class="menu">
                <ul class="sf-menu">
                    <?php foreach (Yii::$app->view->params['menu'] as $v):?>
                        <?php if($v->status):?>
                            <li>
                            <a href="<?=URL::toRoute([$v->url])?>"><?=$v->text?></a>
                            <?php $submenu = $v->menuItems;?>
                            <?php if($submenu != null):?>
                                <ul class="submenu">
                            <?php endif;?>
                                <?php foreach ($submenu as $subv):?>
                                    <?php if($subv->status):?>
                                        <li>
                                            <a href="<?=URL::toRoute([$subv->url])?>"><?=$subv->name?></a>
                                                <?php if(in_array($subv->id,$subarr)):?>
                                                    <?php if($subv->id == 19){?>
                                                        <ul class="submenu" style="left: -100%;">
                                                    <?php }else{?>
                                                        <ul class="submenu">
                                                    <?php }?>

                                                <?php endif;?>
                                                    <?php foreach (Yii::$app->view->params['submenu'] as $subsubv):?>
                                                        <?php if($subsubv->status):?>
                                                            <?php if($subsubv->submenu_id == $subv->id):?>

                                                                    <li><a href="<?=URL::toRoute([$subsubv->url])?>"><?=$subsubv->name?></a></li>

                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                 <?php if(in_array($subv->id,$subarr)):?>
                                                </ul>
                                                <?php endif;?>

                                        </li>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php if($submenu != null):?>
                                </ul>
                            <?php endif;?>
                            </li>
                        <?php endif;?>
                    <?php endforeach;?>
                </ul>
            </nav>
            <!-- END DESKTOP MENU -->
        </div>

    </div>
    <!-- END MENU -->
</header>

<?=$content?>

<!-- FOOTER -->
<footer class="footer" id="footer">

    <div class="footer-top-wrap wow slideInUp">
        <div class="container">

            <div class="footer-top">
                <ul class="footer-links">
                    <?php if(Yii::$app->view->params['menu'][0]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['menu'][0]->url)?>"><?=Yii::$app->view->params['menu'][0]->text?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['menu'][1]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['menu'][1]->url)?>"><?=Yii::$app->view->params['menu'][1]->text?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['smenu'][7]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][7]->url)?>"><?=Yii::$app->view->params['smenu'][7]->name?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['smenu'][15]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][15]->url)?>"><?=Yii::$app->view->params['smenu'][15]->name?></a></li>
                    <?php endif;?>
                </ul>

                <ul class="footer-links">
                    <li><a href="#"><?=Yii::$app->view->params['translation'][27]->name;?></a></li>
                    <?php if(Yii::$app->view->params['menu'][5]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['menu'][5]->url)?>"><?=Yii::$app->view->params['menu'][5]->text?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['menu'][4]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['menu'][4]->url)?>"><?=Yii::$app->view->params['menu'][4]->text?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['menu'][7]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['menu'][7]->url)?>"><?=Yii::$app->view->params['menu'][7]->text?></a></li>
                    <?php endif;?>
                </ul>

                <ul class="footer-links">
                    <?php if(Yii::$app->view->params['smenu'][6]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][6]->url)?>"><?=Yii::$app->view->params['smenu'][6]->name?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['smenu'][4]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][4]->url)?>"><?=Yii::$app->view->params['smenu'][4]->name?></a></li>
                    <?php endif;?>
                    <li><a href="#"><?=Yii::$app->view->params['translation'][28]->name;?></a></li>
                    <li><a href="#"><?=Yii::$app->view->params['translation'][29]->name;?></a></li>
                </ul>

                <ul class="footer-links">
                    <?php if(Yii::$app->view->params['submenu'][1]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['submenu'][1]->url)?>"><?=Yii::$app->view->params['submenu'][1]->name?></a></li>
                    <?php endif;?>
                    <li><a href="#">Госзакупки</a></li>
                    <?php if(Yii::$app->view->params['smenu'][9]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][9]->url)?>"><?=Yii::$app->view->params['smenu'][9]->name?></a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->view->params['smenu'][19]->status):?>
                        <li><a href="<?=Url::toRoute(Yii::$app->view->params['smenu'][19]->url);?>"><?=Yii::$app->view->params['smenu'][19]->name?></a></li>
                    <?php endif;?>
                </ul>

                <div class="footer-contacts">
                    <div class="footer-phone">
                        <img src="/public/images/footer-phone.png" alt="Телефон">
                        <span>
                            <?php foreach ($tel as $v): ?>
                                <a href="tel:<?=$v;?>"><?=$v;?></a>
                            <?php endforeach;?>
                        </span>
                    </div>

                    <div class="footer-message">
                        <img src="/public/images/footer-message.png" alt="Сообщение">
                        <span><a href="mailto:<?=$email[0];?>"><?=$email[0];?></a></span>
                    </div>

                    <div class="footer-location">
                        <img src="/public/images/footer-location.png" alt="Локация">
                        <span><?=Yii::$app->view->params['contact']->address?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-wrap wow slideInUp">

        <div class="container">
            <div class="footer-bottom">
                <div class="footer-bottom-copyright">
                    <p><?=Yii::$app->view->params['copyright']->text?></p>
                </div>

                <div class="developed-by">
                    <a target="_blank" href="https://www.a-lux.kz/"><img src="/public/images/logo_a-lux.png" alt="Разработано в веб студии A-Lux"></a>
                    <a href="https://a-lux.kz/" style="color: #fff; font-size: 12px;" target="_blank">Разработка сайтов в Алматы</a>
                </div>

                <div class="footer-bottom-social">
                    <a href="<?=Yii::$app->view->params['contact']->facebook?>" target="_blank"><img src="/public/images/facebook.png" alt="facebook"></a>
                    <a href="<?=Yii::$app->view->params['contact']->vk?>" target="_blank"><img src="/public/images/vk.png" alt="vk"></a>
                    <a href="<?=Yii::$app->view->params['contact']->instagram?>" target="_blank"><img src="/public/images/instagram.png" alt="instagram"></a>
                    <a href="<?=Yii::$app->view->params['contact']->twitter?>" target="_blank"><img src="/public/images/twitter.png" alt="twitter"></a>
                    <a href="<?=Yii::$app->view->params['contact']->google?>" target="_blank"><img src="/public/images/google.png" alt="google"></a>
                </div>
            </div>
        </div>

    </div>

</footer>
<!-- END FOOTER -->

</body>
</html>


<?php $this->endBody() ?>
</body>
<script type="text/javascript">
    $(function(){
        $("#cube-loader").fadeOut("slow");
    });
</script>
</html>
<?php $this->endPage() ?>
