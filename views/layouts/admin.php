<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <?= Html::a(
                            'Выйти',
                            ['/auth/logout']
                        ) ?>
                    </li>
                    <select class="language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                        <option <?=(Yii::$app->session["lang"]=='')?'selected':''?> value="/lang/index?url=ru">RU</option>
                        <option <?=(Yii::$app->session["lang"]=='_kz')?'selected':''?> value="/lang/index?url=kz">KZ</option>
						<option <?=(Yii::$app->session["lang"]=='_en')?'selected':''?> value="/lang/index?url=en">EN</option>
                    </select>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [

                        ['label' => 'Переводы', 'icon' => 'fa fa-user', 'url' => ['/admin/translation/'],'active' => $this->context->id == 'translation'],
                        [
                            'label' => 'Меню',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/'],'active' => $this->context->id == 'menu'],
                                ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/submenu/'],'active' => $this->context->id == 'submenu'],
                                ['label' => 'Подкатегории ', 'icon' => 'fa fa-user', 'url' => ['/admin/subsubmenu/'],'active' => $this->context->id == 'subsubmenu'],
                            ],
                        ],
                        [
                            'label' => 'Главная страница',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Правый слайдер',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Заголовок', 'icon' => 'fa fa-user', 'url' => ['/admin/slidername/'],'active' => $this->context->id == 'slidername'],
                                        ['label' => 'Содержание', 'icon' => 'fa fa-user', 'url' => ['/admin/slideright/'],'active' => $this->context->id == 'slideright'],
                                    ],
                                ],
                                [
                                    'label' => 'Первый блок',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Содержание', 'icon' => 'fa fa-user', 'url' => ['/admin/mainslidea/'],'active' => $this->context->id == 'mainslidea'],
                                        ['label' => 'Слайдер', 'icon' => 'fa fa-user', 'url' => ['/admin/mainslideaslider/'],'active' => $this->context->id == 'mainslideaslider'],
                                        ['label' => 'Пункты', 'icon' => 'fa fa-user', 'url' => ['/admin/mainslideapage/'],'active' => $this->context->id == 'mainslideapage'],
                                    ],
                                ],
                                ['label' => 'Второй блок ', 'icon' => 'fa fa-user', 'url' => ['/admin/mainslideb/'],'active' => $this->context->id == 'mainslideb'],
                                [
                                    'label' => 'Ближайшие мероприятия ',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Заголовок', 'icon' => 'fa fa-user', 'url' => ['/admin/eventsub/'],'active' => $this->context->id == 'eventsub'],
                                        ['label' => 'Категория', 'icon' => 'fa fa-user', 'url' => ['/admin/eventtypes/'],'active' => $this->context->id == 'eventtypes'],
                                        ['label' => 'Содержания', 'icon' => 'fa fa-user', 'url' => ['/admin/eventcon/'],'active' => $this->context->id == 'eventcon'],
                                    ],
                                ],

                                ['label' => 'Билеты на выставки', 'icon' => 'fa fa-user', 'url' => ['/admin/mainslidec/'],'active' => $this->context->id == 'mainslidec'],
                                [
                                    'label' => 'Новости ',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Заголовок', 'icon' => 'fa fa-user', 'url' => ['/admin/newssub/'],'active' => $this->context->id == 'newssub'],
                                    ],
                                ],


                            ],
                        ],

                        ['label' => 'Страницы (заг. и б.)', 'icon' => 'fa fa-user', 'url' => ['/admin/subtitle/'],'active' => $this->context->id == 'subtitle'],

                        [
                            'label' => 'О центре',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Задача, цели и мандат', 'icon' => 'fa fa-user', 'url' => ['/admin/mandat/'],'active' => $this->context->id == 'mandat'],
                                [
                                    'label' => 'Организационная структура',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/strucsub/'],'active' => $this->context->id == 'strucsub'],
                                        ['label' => 'Администрация ', 'icon' => 'fa fa-user', 'url' => ['/admin/strucadmin/'],'active' => $this->context->id == 'strucadmin'],
                                        ['label' => 'Биография ', 'icon' => 'fa fa-user', 'url' => ['/admin/biography/'],'active' => $this->context->id == 'biography'],
                                        ['label' => 'Отделы', 'icon' => 'fa fa-user', 'url' => ['/admin/struccon/'],'active' => $this->context->id == 'struccon'],
                                    ],
                                ],
                                ['label' => 'Отчеты ', 'icon' => 'fa fa-user', 'url' => ['/admin/report/'],'active' => $this->context->id == 'report'],
                            ],
                        ],


                        [
                            'label' => 'Мероприятия/Проекты',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Грантовые проекты',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Проекты', 'icon' => 'fa fa-user', 'url' => ['/admin/grant/'],'active' => $this->context->id == 'grant'],
                                        ['label' => 'Картинки проекта ', 'icon' => 'fa fa-user', 'url' => ['/admin/grantpic/'],'active' => $this->context->id == 'grantpic'],
                                    ],
                                ],

                                [
                                    'label' => 'Конференции',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Конференции', 'icon' => 'fa fa-user', 'url' => ['/admin/conference/'],'active' => $this->context->id == 'conference'],
                                        ['label' => 'Картинки конференции ', 'icon' => 'fa fa-user', 'url' => ['/admin/conferencepic/'],'active' => $this->context->id == 'conferencepic'],
                                    ],
                                ],

                                [
                                    'label' => 'Форумы',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Форумы', 'icon' => 'fa fa-user', 'url' => ['/admin/forum/'],'active' => $this->context->id == 'forum'],
                                        ['label' => 'Картинки Форума', 'icon' => 'fa fa-user', 'url' => ['/admin/forumpic/'],'active' => $this->context->id == 'forumpic'],
                                    ],
                                ],

                                [
                                    'label' => 'Выставки',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Выставки', 'icon' => 'fa fa-user', 'url' => ['/admin/exhibition/'],'active' => $this->context->id == 'exhibition'],
                                        ['label' => 'Картинки выставки', 'icon' => 'fa fa-user', 'url' => ['/admin/exhibitionpic/'],'active' => $this->context->id == 'exhibitionpic'],
                                    ],
                                ],

                                [
                                    'label' => 'Публикации',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        [
                                            'label' => 'Статьи, интервью',
                                            'icon' => 'fa fa-user',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Подкатегории', 'icon' => 'fa fa-user', 'url' => ['/admin/articlesub/'],'active' => $this->context->id == 'articlesub'],
                                                ['label' => 'Содержании', 'icon' => 'fa fa-user', 'url' => ['/admin/articlecon/'],'active' => $this->context->id == 'articlecon'],
                                            ],
                                        ],

                                        [
                                            'label' => 'Книги',
                                            'icon' => 'fa fa-user',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Подкатегории', 'icon' => 'fa fa-user', 'url' => ['/admin/booksub/'],'active' => $this->context->id == 'booksub'],
                                                ['label' => 'Содержании', 'icon' => 'fa fa-user', 'url' => ['/admin/bookin/'],'active' => $this->context->id == 'bookin'],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],

                        ['label' => 'Партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partner/index'],'active' => $this->context->id == 'partner'],

                        [
                            'label' => 'Новости',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'События', 'icon' => 'fa fa-user', 'url' => ['/admin/event/'],'active' => $this->context->id == 'event'],
                                ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/admin/mainnewsa/'],'active' => $this->context->id == 'mainnewsa'],
                            ],
                        ],


                        [
                            'label' => 'Медиа',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Галерея', 'icon' => 'fa fa-user', 'url' => ['/admin/gallery/'],'active' => $this->context->id == 'gallery'],
                                [
                                    'label' => 'СМИ о нас',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Подкатегории', 'icon' => 'fa fa-user', 'url' => ['/admin/mastab/'],'active' => $this->context->id == 'mastab'],
                                        ['label' => 'Содержании', 'icon' => 'fa fa-user', 'url' => ['/admin/mastabcon/'],'active' => $this->context->id == 'mastabcon'],
                                    ],
                                ],
                                [
                                    'label' => 'Пресс релизы',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Пресс релизы', 'icon' => 'fa fa-user', 'url' => ['/admin/press/'],'active' => $this->context->id == 'press'],
                                        ['label' => 'Картинки пресс релизы', 'icon' => 'fa fa-user', 'url' => ['/admin/presspic/'],'active' => $this->context->id == 'presspic'],
                                    ],
                                ],
                            ],
                        ],

                        [
                            'label' => 'Экспозиция',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Структура ', 'icon' => 'fa fa-user', 'url' => ['/admin/exstrucadmin/'],'active' => $this->context->id == 'exstrucadmin'],
//                                [
//                                    'label' => 'Структура',
//                                    'icon' => 'fa fa-user',
//                                    'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Административный департамент', 'icon' => 'fa fa-user', 'url' => ['/admin/exstrucadmin/'],'active' => $this->context->id == 'exstrucadmin'],
////                                        ['label' => 'Заголовок департаментов', 'icon' => 'fa fa-user', 'url' => ['/admin/exstrucdepsub/'],'active' => $this->context->id == 'exstrucdepsub'],
//                                        ['label' => 'Содержании департаментов', 'icon' => 'fa fa-user', 'url' => ['/admin/exstrucdepcon/'],'active' => $this->context->id == 'exstrucdepcon'],
//                                    ],
//                                ],

                                [
                                    'label' => 'Коллекции',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Коллекции', 'icon' => 'fa fa-user', 'url' => ['/admin/collection/'],'active' => $this->context->id == 'collection'],
                                        ['label' => 'Дополнительные картинки', 'icon' => 'fa fa-user', 'url' => ['/admin/collectionpic/'],'active' => $this->context->id == 'collectionpic'],
                                    ],
                                ],
                                ['label' => '3D Тур ', 'icon' => 'fa fa-user', 'url' => ['/admin/tour/'],'active' => $this->context->id == 'tour'],

                                [
                                    'label' => '«Великий Шелковый путь»',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        [
                                            'label' => 'В Казахстане',
                                            'icon' => 'fa fa-user',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Содержание', 'icon' => 'fa fa-user', 'url' => ['/admin/intermap/'],'active' => $this->context->id == 'intermap'],
                                                ['label' => 'Города', 'icon' => 'fa fa-user', 'url' => ['/admin/maps/'],'active' => $this->context->id == 'maps'],
                                                ['label' => 'Слайдеры', 'icon' => 'fa fa-user', 'url' => ['/admin/map-sliders/'],'active' => $this->context->id == 'map-sliders'],
                                            ],
                                        ],
                                        ['label' => 'В мире', 'icon' => 'fa fa-user', 'url' => ['/admin/worlds/'],'active' => $this->context->id == 'worlds'],
                                    ],
                                ],
                                ['label' => 'Сувенирная продукция', 'icon' => 'fa fa-user', 'url' => ['/admin/souvenir/'],'active' => $this->context->id == 'souvenir'],
                                [
                                    'label' => 'Прейскурант',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Названия колонка', 'icon' => 'fa fa-user', 'url' => ['/admin/pricelistsub/'],'active' => $this->context->id == 'pricelistsub'],
                                        ['label' => 'Содержания', 'icon' => 'fa fa-user', 'url' => ['/admin/pricelist/'],'active' => $this->context->id == 'pricelist'],
                                    ],
                                ],





                            ],
                        ],

                        [
                            'label' => 'Контакты',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Контакты ', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/'],'active' => $this->context->id == 'contact'],
                                [
                                    'label' => 'Обратная связь',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [

                                        ['label' => 'Подзаголовок', 'icon' => 'fa fa-user', 'url' => ['/admin/feedbacksub/'],'active' => $this->context->id == 'feedbacksub'],
                                        ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/'],'active' => $this->context->id == 'emailforrequest'],
                                        ['label' => 'Заявки', 'icon' => 'fa fa-user', 'url' => ['/admin/feedback/'],'active' => $this->context->id == 'feedback'],
                                    ],
                                ],
                                ['label' => 'Вакансии', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy/'],'active' => $this->context->id == 'vacancy'],
                                ['label' => 'Блог директора', 'icon' => 'fa fa-user', 'url' => ['/admin/directorblog/'],'active' => $this->context->id == 'directorblog'],
                            ],
                        ],
                        ['label' => 'Согласование', 'icon' => 'fa fa-user', 'url' => ['/admin/agreement/'],  'active' => $this->context->id == 'agreement'],
                        ['label' => 'Копирайт', 'icon' => 'fa fa-user', 'url' => ['/admin/copyright/'],'active' => $this->context->id == 'copyright'],
                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
