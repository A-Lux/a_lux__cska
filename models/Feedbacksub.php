<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedbacksub".
 *
 * @property int $id
 * @property string $text
 * @property string $buttonName
 */
class Feedbacksub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedbacksub'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'buttonName'], 'required'],
            [['text', 'buttonName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Название',
            'buttonName' => 'Название кнопки',
        ];
    }
}
