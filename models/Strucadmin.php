<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "strucadmin".
 *
 * @property int $id
 * @property string $text
 * @property string $position
 */
class Strucadmin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'strucadmin'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'position'], 'required'],
            [['position'], 'string'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'ФИО,Телефон',
            'position' => 'Должность',
        ];
    }
}
