<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "struccon".
 *
 * @property int $id
 * @property string $position
 * @property string $phone
 * @property string $content
 * @property int $strucsub_id
 */
class Struccon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'struccon'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'phone', 'content', 'strucsub_id'], 'required'],
            [['content'], 'string'],
            [['strucsub_id'], 'integer'],
            [['position', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'ФИО',
            'phone' => 'Номер телефон',
            'content' => 'Содержания',
            'strucsub_id' => 'Отдел',
        ];
    }

    public function getSub()
    {
        return $this->hasOne(Strucsub::className(), ['id' => 'strucsub_id']);
    }

    public function getSubName(){
        return (isset($this->sub))? $this->sub->text:'Не задан';
    }
}
