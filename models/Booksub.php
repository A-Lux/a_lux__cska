<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "booksub".
 *
 * @property int $id
 * @property int $name
 */
class Booksub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booksub'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Booksub::find()->all(),'id','name');
    }

    public function getContentItems(){
        return $this->hasMany(Bookin::className(), ['booksub_id' => 'id']);
    }

    public static function getAll($booksub_id, $pageSize=4)
    {

        $query = Bookin::find()->where('booksub_id='.$booksub_id);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
