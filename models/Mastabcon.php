<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mastabcon".
 *
 * @property int $id
 * @property string $content
 * @property int $mastab_id
 */
class Mastabcon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mastabcon'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'mastab_id'], 'required'],
            [['mastab_id'], 'integer'],
            [['content'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'mastab_id' => 'Подкатегории',
        ];
    }

    public function getCon()
    {
        return $this->hasOne(Mastab::className(), ['id' => 'mastab_id']);
    }

    public function getContentName(){
        return (isset($this->con))? $this->con->name:'Не задан';
    }
}
