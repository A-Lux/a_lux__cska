<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventtypes".
 *
 * @property int $id
 * @property string $text
 */
class Eventtypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventtypes'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Eventtypes::find()->all(),'id','text');
    }

    public function getEventItems(){
        return $this->hasMany(Eventcon::className(), ['eventtypes_id' => 'id']);
    }
}
