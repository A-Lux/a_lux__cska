<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slideright".
 *
 * @property int $id
 * @property string $text
 * @property static $url
 */
class Slideright extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slideright'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text','url'], 'required'],
            [['text', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Название',
            'url' => 'Ссылка'
        ];
    }
}
