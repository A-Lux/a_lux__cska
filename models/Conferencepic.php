<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conferencepic".
 *
 * @property int $id
 * @property string $image
 * @property int $conference_id
 */
class Conferencepic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conferencepic'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['conference_id'], 'required'],
            [['conference_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg','maxFiles' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'conference_id' => 'Конференции',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }


    public function getContent()
    {
        return $this->hasOne(Conference::className(), ['id' => 'conference_id']);
    }

    public function getContentName(){
        return (isset($this->content))? $this->content->text:'Не задан';
    }
}
