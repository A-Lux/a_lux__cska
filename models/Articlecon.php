<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articlecon".
 *
 * @property int $id
 * @property string $content
 * @property int $articlesub_id
 */
class Articlecon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articlecon'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'articlesub_id'], 'required'],
            [['content'], 'string'],
            [['articlesub_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'articlesub_id' => 'Подкатегории',
        ];
    }

    public function getCon()
    {
        return $this->hasOne(Articlesub::className(), ['id' => 'articlesub_id']);
    }

    public function getContentName(){
        return (isset($this->con))? $this->con->name:'Не задан';
    }
}
