<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exstrucdepsub".
 *
 * @property int $id
 * @property string $name
 */
class Exstrucdepsub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exstrucdepsub'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Exstrucdepsub::find()->all(),'id','name');
    }

    public function getDepItems(){
        return $this->hasMany(Exstrucdepcon::className(), ['exstrucdepsub_id' => 'id']);
    }
}
