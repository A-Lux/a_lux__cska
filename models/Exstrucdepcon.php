<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exstrucdepcon".
 *
 * @property int $id
 * @property string $position
 * @property string $phone
 * @property string $content
 * @property int $exstrucdepsub_id
 */
class Exstrucdepcon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exstrucdepcon'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'phone', 'content', 'exstrucdepsub_id'], 'required'],
            [['content'], 'string'],
            [['exstrucdepsub_id'], 'integer'],
            [['position', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'ФИО',
            'phone' => 'Номер телефон',
            'content' => 'Содержания',
            'exstrucdepsub_id' => 'Департамент',
        ];
    }

    public function getSub()
    {
        return $this->hasOne(Exstrucdepsub::className(), ['id' => 'exstrucdepsub_id']);
    }

    public function getSubName(){
        return (isset($this->sub))? $this->sub->name:'Не задан';
    }
}
