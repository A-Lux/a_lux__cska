<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "strucsub".
 *
 * @property int $id
 * @property string $text
 */
class Strucsub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'strucsub'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Strucsub::find()->where("id>1")->all(),'id','text');
    }

    public function getDepItems(){
        return $this->hasMany(Struccon::className(), ['strucsub_id' => 'id']);
    }
}
