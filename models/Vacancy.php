<?php

namespace app\models;

use app\modules\admin\controllers\VacancyController;
use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "vacancy".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property string $price
 * @property string $address
 * @property string $date
 * @property string $employment
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content','name', 'employment'], 'required'],
            [['date'], 'default', 'value' => date('Y-m-d')],
            [['name', 'price', 'address', 'employment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Должность ',
            'content' => 'Содержание',
            'price' => 'Зарплата',
            'address' => 'Адрес',
            'date' => 'Дата',
            'employment' => 'Занятость',
        ];
    }

    public static function getAll($pageSize=4)
    {
        Yii::$app->view->params['about'] = "vacancy";
        $query = Vacancy::find()->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $data = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $data['data'] = $data;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }
}
