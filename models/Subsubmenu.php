<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subsubmenu".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $submenu_id
 */
class Subsubmenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subsubmenu'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status', 'submenu_id'], 'required'],
            [['status', 'submenu_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'submenu_id' => ' Подкатегории',
        ];
    }

    public function getMenu()
    {
        return $this->hasOne(Submenu::className(), ['id' => 'submenu_id']);
    }

    public function getMenuName(){
        return (isset($this->menu))? $this->menu->name:'Не задан';
    }
}
