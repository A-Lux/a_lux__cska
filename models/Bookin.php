<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookin".
 *
 * @property int $id
 * @property string $author
 * @property string $name
 * @property string $content
 * @property string $image
 * @property int $booksub_id
 */
class Bookin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bookin'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author', 'name', 'content', 'booksub_id'], 'required'],
            [['content'], 'string'],
            [['booksub_id'], 'integer'],
            [['author', 'name'], 'string', 'max' => 255],
            [['image'],'file','extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Автор',
            'name' => 'Наименование',
            'content' => 'Описание книги',
            'image' => 'Картинка',
            'booksub_id' => 'Год выпуска',
        ];
    }

    public function getCon()
    {
        return $this->hasOne(Booksub::className(), ['id' => 'booksub_id']);
    }

    public function getContentName(){
        return (isset($this->con))? $this->con->name:'Не задан';
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
