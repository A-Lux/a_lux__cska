<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exhibitionpic".
 *
 * @property int $id
 * @property string $image
 * @property int $exhibition_id
 */
class Exhibitionpic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exhibitionpic'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'exhibition_id'], 'required'],
            [['exhibition_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg','maxFiles' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'exhibition_id' => 'Выставки',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }


    public function getContent()
    {
        return $this->hasOne(Exhibition::className(), ['id' => 'exhibition_id']);
    }

    public function getContentName(){
        return (isset($this->content))? $this->content->text:'Не задан';
    }
}
