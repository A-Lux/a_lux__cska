<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "mastab".
 *
 * @property int $id
 * @property string $name
 */
class Mastab extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mastab'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Mastab::find()->all(),'id','name');
    }

    public function getContentItems(){
        return $this->hasMany(Mastabcon::className(), ['mastab_id' => 'id']);
    }

    public static function getAll($mastab_id, $pageSize=6)
    {

        $query = Mastabcon::find()->where('mastab_id='.$mastab_id);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
