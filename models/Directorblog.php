<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directorblog".
 *
 * @property int $id
 * @property string $image
 * @property string $content
 * @property string $buttonName
 */
class Directorblog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directorblog'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'buttonName'], 'required'],
            [['content'], 'string'],
            [['image', 'buttonName'], 'string', 'max' => 255],
            [['image'],'file','extensions'=>'jpg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'content' => 'Содержания',
            'buttonName' => 'Названия кнопка',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

}
