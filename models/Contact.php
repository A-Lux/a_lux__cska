<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $title
 * @property string $telephone
 * @property string $email
 * @property string $address
 * @property string $facebook
 * @property string $vk
 * @property string $instagram
 * @property string $twitter
 * @property string $google
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'telephone', 'email', 'address', 'facebook', 'vk', 'instagram', 'twitter', 'google'], 'required'],
            [['title', 'telephone', 'email', 'address', 'facebook', 'vk', 'instagram', 'twitter', 'google'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'telephone' => 'Телефон',
            'email' => 'Email',
            'address' => 'Адрес',
            'facebook' => 'URL-адрес Facebook ',
            'vk' => 'URL-адрес Vk',
            'instagram' => 'URL-адрес Instagram',
            'twitter' => 'URL-адрес Twitter',
            'google' => 'URL-адрес Google',
        ];
    }
}
