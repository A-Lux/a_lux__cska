<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $content
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'content'], 'required'],
            [['content'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'],'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Эл. адрес',
            'content' => 'Сообщения',
        ];
    }

    public function saveRequest($name,$email,$content){
        $this->name = $name;
        $this->email = $email;
        $this->content = $content;
        return $this->save(false);
    }
}
