<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exstrucadmin".
 *
 * @property int $id
 * @property string $text
 * @property string $content
 */
class Exstrucadmin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exstrucadmin'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'content'], 'required'],
            [['content'], 'string'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Название',
            'content' => 'Содержание',
        ];
    }
}
