<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "collection".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $date
 * @property string $content
 */
class Collection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['name', 'content'], 'string'],
            [['date'], 'safe'],
            [['image'],'file','extensions'=>'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'date' => 'Дата',
            'content' => 'Содержание',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
        Collectionpic::deleteAll(['collection_id' => $this->id]);
    }

    public function beforeDelete()
    {
        $this->deleteImage();

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Collection::find()->all(),'id','name');
    }

    public static function getAll($pageSize=8)
    {


        $query = Collection::find()->orderBy('date desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getDate()
    {
        if(Yii::$app->session["lang"] == '_en'){
            $lang = 'en-EN';
        }elseif(Yii::$app->session["lang"] == '_kz'){
            $lang = 'kaz-KAZ';
        }else{
            $lang = 'ru-RU';
        }

        Yii::$app->formatter->locale = $lang;
        return Yii::$app->formatter->asDate($this->date, 'long');
    }
}
