<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pricelistsub".
 *
 * @property int $id
 * @property string $contenta
 * @property string $unit
 * @property string $contentb
 */
class Pricelistsub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pricelistsub'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contenta', 'unit', 'contentb'], 'required'],
            [['contenta', 'unit', 'contentb'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contenta' => 'Названия услуга',
            'unit' => 'Единица измерения',
            'contentb' => 'Цена',
        ];
    }
}
