<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mainslideapage".
 *
 * @property int $id
 * @property string $text
 * @property string $url
 */
class Mainslideapage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mainslideapage'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'url'], 'required'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'url' => 'Ссылка'
        ];
    }
}
