<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "submenu".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $menu_id
 */
class Submenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'submenu'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status', 'menu_id'], 'required'],
            [['status', 'menu_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'menu_id' => 'Категория',
        ];
    }


    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    public function getMenuName(){
        return (isset($this->menu))? $this->menu->text:'Не задан';
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Submenu::find()->all(),'id','name');
    }

    public function getMenuItems(){
        return $this->hasMany(Subsubmenu::className(), ['submenu_id' => 'id']);
    }
}
