<?php

namespace app\controllers;

use app\models\Articlesub;
use app\models\Biography;
use app\models\Bookin;
use app\models\Booksub;
use app\models\Collection;
use app\models\Collectionpic;
use app\models\Conference;
use app\models\Conferencepic;
use app\models\Contact;
use app\models\Copyright;
use app\models\Directorblog;
use app\models\Emailforrequest;
use app\models\Event;
use app\models\Eventcon;
use app\models\Eventsub;
use app\models\Eventtypes;
use app\models\Exhibition;
use app\models\Exhibitionpic;
use app\models\Exstrucadmin;
use app\models\Exstrucdepsub;
use app\models\Feedback;
use app\models\Feedbacksub;
use app\models\Forum;
use app\models\Forumpic;
use app\models\Gallery;
use app\models\Grant;
use app\models\Grantpic;
use app\models\Intermap;
use app\models\Mainnewsa;
use app\models\Mainslidea;
use app\models\Mainslideapage;
use app\models\Mainslideaslider;
use app\models\Mainslideb;
use app\models\Mainslidec;
use app\models\Mandat;
use app\models\Maps;
use app\models\MapSliders;
use app\models\Mastab;
use app\models\Menu;
use app\models\Newssub;
use app\models\Partner;
use app\models\Press;
use app\models\Presspic;
use app\models\Pricelist;
use app\models\Pricelistsub;
use app\models\Report;
use app\models\Slideright;
use app\models\Slidername;
use app\models\Souvenir;
use app\models\Strucadmin;
use app\models\Struccon;
use app\models\Strucsub;
use app\models\Submenu;
use app\models\Subsubmenu;
use app\models\Subtitle;
use app\models\Tour;
use app\models\Translation;
use app\models\Vacancy;
use app\models\Worlds;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\widgets\Block;
use app\models\Agreement;

class SiteController extends FrontendController
{




    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    protected function sendEmail($name, $email, $message) {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom('robot@icrc.kz')
            ->setTo(Yii::$app->view->params['email']->email)
            ->setSubject('Клиент хочет связаться с вами')

            ->setHtmlBody("<p>Имя: $name</p>
                                 <p>Email: $email</p>
                                 <p>Сообщения: $message</p>");
        return $emailSend->send();

    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $sliderCon = Slideright::find()->all();
        $sliderName = Slidername::find()->all();
        $bloka['content'] = Mainslidea::find()->all();
        $bloka['slider'] = Mainslideaslider::find()->all();
        $bloka['types'] = Mainslideapage::find()->all();
        $blokb = Mainslideb::find()->all();
        $eventTitle = Eventsub::find()->all();
        $events = Eventtypes::find()->with('eventItems')->all();
        $eventCon = Eventcon::find()->all();
        $blokc = Mainslidec::find()->one();
        $news['sub'] = Newssub::find()->one();
        $news['newa'] = Mainnewsa::find()->where(['in_main' => 1])->orderBy('date desc')->limit(3)->all();
        $news['news'] = Mainnewsa::find()->where(['in_main' => 1])->orderBy('date desc')->all();
        $contact = Contact::find()->one();
        return $this->render('index',compact('bloka','blokb','sliderCon','sliderName','events','eventTitle',
            'eventCon','blokc','news','contact'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $feedsub = Feedbacksub::find()->one();
        if($feedsub){
            return $this->render('contact',compact('feedsub'));
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionRequest($name,$email,$content)
    {

        $model = new Feedback();

        if(Yii::$app->request->isGet){

            if($model->saveRequest($name,$email,$content) && $this->sendEmail($name,$email,$content))
            {
                if(Yii::$app->session["lang"] == "") {
                    Yii::$app->getSession()->setFlash('request', 'Спасибо, скоро мы свяжемся с вами!');
                }else{
                    Yii::$app->getSession()->setFlash('request', 'Рахмет, біз сізге жақын арада хабарласамыз!');
                }
                return $this->redirect(['site/contact']);
            }

        }
    }




    public function actionMandat()
    {
        $mandat = Mandat::find()->one();
        if($mandat) {
            return $this->render('mandat', compact('mandat'));
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionStructure()
    {
        $sub = Strucsub::find()->with('depItems')->where("id>1")->all();
        $admin = Strucadmin::find()->all();
        $admintitle = Strucsub::find()->one();
        return $this->render('structure',compact('sub','admin','admintitle'));
    }


    public function actionDirector()
    {

        $blog = Directorblog::find()->one();
        return $this->render('director',compact('blog'));
    }



    public function actionReport()
    {

        $report = Report::find()->all();
        return $this->render('report',compact('report'));
    }


    public function actionGrant()
    {
        $projects = Grant::getAll(3);
        return $this->render('grant',compact('projects'));
    }

    public function actionProjectview($id)
    {
        $content = Grant::findOne($id);
        if($content) {
            $pictures = Grantpic::find()->where('grant_id=' . $id)->all();
            return $this->render('grantin', compact('content', 'pictures'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionConference()
    {

        $conf = Conference::getAll();
        return $this->render('conference',compact('conf'));
    }

    public function actionConferenceview($id)
    {
        $content = Conference::findOne($id);
        if($content) {
            $pictures = Conferencepic::find()->where('conference_id=' . $id)->all();
            return $this->render('conferencein', compact('content', 'pictures'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionForum()
    {

        $forum = Forum::getAll();
        return $this->render('forum',compact('forum'));
    }

    public function actionForumview($id)
    {
        $content = Forum::findOne($id);
        if($content) {
            $pictures = Forumpic::find()->where('forum_id=' . $id)->all();
            return $this->render('forumin', compact('content', 'pictures'));
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionExhibition()
    {

        $exhibition = Exhibition::getAll();
        return $this->render('exhibition',compact('exhibition'));
    }

    public function actionExhibitionview($id)
    {
        $content = Exhibition::findOne($id);
        $pictures = Exhibitionpic::find()->where('exhibition_id='.$id)->all();
        return $this->render('exhibitionin',compact('content','pictures'));
    }

    public function actionVacancy(){

        $vacancies  = Vacancy::getAll(4);
        return $this->render('vacancy',compact('vacancies'));
    }

    public  function actionPartner(){
        $partners = Partner::getAll();

        return $this->render('partner',compact('partners'));
    }



    public function actionNews(){

        $news = Mainnewsa::getAll();
        $rightNews = Mainnewsa::find()->orderBy('date desc')->all();
        return $this->render('news',compact('news','rightNews'));
    }

    public function actionGallery(){

        $galleries = Gallery::getAll();
        return $this->render('gallery',compact('galleries'));
    }


    public function actionMasmedia(){

        $data = Mastab::find()->orderBy('name desc')->all();
        $n = 0;
        foreach ($data as $v){
            $mastab[$n]['id'] = $v->id;
            $mastab[$n]['title'] = $v->name;
            $mastab[$n]['content'] = Mastab::getAll($v->id,6);
            $n++;
        }

        return $this->render('masmedia',compact('mastab'));
    }



    public function actionPress(){

        $press = Press::getAll();
        return $this->render('press',compact('press'));
    }


    public function actionPressview($id)
    {
        $content = Press::findOne($id);
        if($content) {
            $pictures = Presspic::find()->where('press_id=' . $id)->all();
            return $this->render('pressin', compact('content', 'pictures'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionExstructure(){

        $exstrucadmin = Exstrucadmin::find()->all();
        $exstrucdep = Exstrucdepsub::find()->with('depItems')->all();
        return $this->render('exstructure',compact('exstrucadmin','exstrucdep'));
    }


    public function actionCollection(){

        $collection = Collection::getAll();
        return $this->render('collection',compact('collection'));
    }

    public function actionCollectionview($id)
    {
        $content = Collection::findOne($id);
        if($content) {
            $pictures = Collectionpic::find()->where('collection_id=' . $id)->all();
            return $this->render('collectionin', compact('content', 'pictures'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionSouvenir()
    {
        $souvenir = Souvenir::getAll();
        return $this->render('souvenir',compact('souvenir'));
    }

    public function actionSouvenirview($id)
    {
        $content = Souvenir::findOne($id);
        if($content) {
            return $this->render('souvenirin', compact('content'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionPricelist()
    {
        $pricesub = Pricelistsub::find()->one();
        $pricecon = Pricelist::find()->all();

        if($pricesub) {
            return $this->render('pricelist', compact('pricesub', 'pricecon'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionTour()
    {
        $tour = Tour::find()->one();
        if($tour) {
            return $this->render('tour', compact('tour'));
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionArticle()
    {
        $data = Articlesub::find()->orderBy('name desc')->all();
        $n = 0;
        foreach ($data as $v){
            $article[$n]['id'] = $v->id;
            $article[$n]['title'] = $v->name;
            $article[$n]['content'] = Articlesub::getAll($v->id,6);
            $n++;
        }
        return $this->render('article',compact('article'));

    }


    public function actionBook()
    {

        $data = Booksub::find()->orderBy('name desc')->all();
        $n = 0;
        foreach ($data as $v){
            $books[$n]['id'] = $v->id;
            $books[$n]['title'] = $v->name;
            $books[$n]['content'] = Booksub::getAll($v->id,4);
            $n++;
        }
        return $this->render('book',compact('books'));
    }

    public function actionBiography(){

        $model = Biography::find()->one();
        if($model) {
            return $this->render('biography', compact('model'));
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionBookview($id)
    {
        $content = Bookin::findOne($id);
        if($content) {
            return $this->render('bookin', compact('content'));
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionMap()
    {
        $map = Intermap::find()->one();
        if($map){

            $cities = Maps::find()->all();
            return $this->render('map',compact('map', 'cities'));
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionCity($id)
    {
        $city = Maps::findOne($id);
        if($city){
            $sliders = MapSliders::getByMap($city->id);
            return $this->render('mapin',compact( 'city', 'sliders'));
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionWorld(){

        $model = Worlds::getContent();
        if($model){
            return $this->render('world', compact('model'));
        }else{
            throw new NotFoundHttpException();
        }

    }

}
