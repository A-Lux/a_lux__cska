<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.06.2020
 * Time: 16:41
 */

namespace app\controllers;


use app\models\Event;
use yii\web\NotFoundHttpException;

class EventController extends FrontendController
{
    public function actionIndex(){

        $events = Event::getAll();
        $rightEvent = Event::getLastEvents();

        return $this->render('index', compact('events','rightEvent'));
    }

    public function actionView($id){

        $event = Event::findOne($id);
        if($event){
            return $this->render('view', compact('event'));
        }else{
            throw new NotFoundHttpException();
        }
    }
}
