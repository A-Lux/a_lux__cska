<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.06.2020
 * Time: 16:40
 */

namespace app\controllers;


use app\models\Agreement;
use app\models\Contact;
use app\models\Copyright;
use app\models\Emailforrequest;
use app\models\Mainslidea;
use app\models\Menu;
use app\models\Submenu;
use app\models\Subsubmenu;
use app\models\Subtitle;
use app\models\Translation;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function init()
    {
        $menu = Menu::find()->with('menuItems')->all();
        $submenu = Subsubmenu::find()->all();
        $smenu = Submenu::find()->all();
        $contact = Contact::find()->one();
        $sub = Subtitle::find()->all();
        $copyright = Copyright::find()->one();
        $translation = Translation::find()->all();
        $email = Emailforrequest::find()->one();
        $agreement = Agreement::find()->one();
        $mainslidea = Mainslidea::find()->one();


        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['submenu'] = $submenu;
        Yii::$app->view->params['smenu'] = $smenu;
        Yii::$app->view->params['contact'] = $contact;
        Yii::$app->view->params['sub'] = $sub;
        Yii::$app->view->params['copyright'] = $copyright;
        Yii::$app->view->params['translation'] = $translation;
        Yii::$app->view->params['email'] = $email;
        Yii::$app->view->params['agreement'] = $agreement;
        Yii::$app->view->params['mainslidea'] = $mainslidea;
    }

}
